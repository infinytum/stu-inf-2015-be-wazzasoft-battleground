package net.battleground;

import net.battleground.listener.*;
import net.battleground.network.PacketManager;

/**
 * Created by bteusm on 16.03.2016.
 */
public class Tester {

    public static void main(String[] args) {
        PacketManager.registerListener(new LoginListener());
        PacketManager.registerListener(new RegisterListener());
        PacketManager.registerListener(new ChannelListener());
        PacketManager.registerListener(new StatisticListener());
        PacketManager.registerListener(new BattleListener());
        PacketManager.registerListener(new ShopListener());
        PacketManager.registerListener(new AsleacListener());
        try {
            new Server().run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
