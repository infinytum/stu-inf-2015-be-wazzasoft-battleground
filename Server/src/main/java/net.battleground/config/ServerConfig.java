package net.battleground.config;

import java.io.File;
import java.io.IOException;

/**
 * Created by bteusm on 31.03.2016.
 */
public class ServerConfig extends Configuration {


    public ServerConfig() {
        super("server.properties");

        File file = new File("server.properties");
        if (!file.exists()) {
            try {
                file.createNewFile();
                load();
                set("DBHost", "localhost");
                set("DBUser", "root");
                set("DBPass", "pa$$word");
                save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getDatabaseHost() {
        return get("DBHost");
    }

    public String getDatabaseUsername() {
        return get("DBUser");
    }

    public String getDatabasePassword() {
        return get("DBPass");
    }
}
