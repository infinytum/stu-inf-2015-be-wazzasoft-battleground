package net.battleground.controller;

import net.battleground.Logger;
import net.battleground.entity.Asleac;
import net.battleground.model.AccountModel;
import net.battleground.model.AsleacModel;
import net.battleground.network.PacketManager;
import net.battleground.packet.BattleInfoPacket;
import net.battleground.server.ServerPlayer;

import java.util.HashMap;

/**
 * Created by bteusm on 24.03.2016.
 */
public class BattleController extends Controller {

    private static ServerPlayer waitingPlayer;
    private static HashMap<Integer, BattleController> runningGames = new HashMap<>();
    private ServerPlayer battlePlayer1;
    private ServerPlayer battlePlayer2;
    private Asleac battleAsleac1;
    private Asleac battleAsleac2;

    public BattleController(ServerPlayer player1, ServerPlayer player2) {
        this.battlePlayer1 = player1;
        this.battlePlayer2 = player2;
        this.battleAsleac1 = player1.getPlayer().getAsleacs().get(0);
        this.battleAsleac2 = player2.getPlayer().getAsleacs().get(0);
        runningGames.put(player1.getId(), this);
        runningGames.put(player2.getId(), this);

        BattleInfoPacket packet = new BattleInfoPacket(BattleInfoPacket.BattleState.BEGIN, battleAsleac2, battleAsleac1, player2.getPlayer().getUsername());
        PacketManager.sendPacket(packet, player1.getChannel());
        packet = new BattleInfoPacket(BattleInfoPacket.BattleState.BEGIN, battleAsleac1, battleAsleac2, player1.getPlayer().getUsername());
        PacketManager.sendPacket(packet, player2.getChannel());
        Logger.info(player1.getPlayer().getUsername() + " and " + player2.getPlayer().getUsername() + " started a Battle!");
    }

    public static void setWaiting(ServerPlayer player) {
        if (BattleController.waitingPlayer != null) {
            new BattleController(waitingPlayer, player);
            waitingPlayer = null;
        } else {
            waitingPlayer = player;
        }
    }

    public static boolean isWaiting(ServerPlayer player) {
        if (waitingPlayer != null)
            return waitingPlayer.getId() == player.getId();
        return false;
    }

    public static void quitWaiting(ServerPlayer player) {
        if (waitingPlayer != null && waitingPlayer.equals(player))
            waitingPlayer = null;
    }

    public static BattleController getRunningBattle(ServerPlayer player) {
        if (BattleController.runningGames.containsKey(player.getId()))
            return BattleController.runningGames.get(player.getId());
        return null;
    }

    public ServerPlayer getTargetPlayer(ServerPlayer attacker) {
        if (battlePlayer1.equals(attacker))
            return battlePlayer2;
        return battlePlayer1;
    }

    public void lose(ServerPlayer player) {
        if (player.getId() == battlePlayer1.getId()) {
            end(battlePlayer2, battlePlayer1);
            return;
        }
        end(battlePlayer1, battlePlayer2);
    }

    public void end(ServerPlayer winner, ServerPlayer loser) {
        int winnerId = winner.getPlayer().getId();
        int loserId = loser.getPlayer().getId();
        AccountModel.setMoney(winner.getPlayer().getId(), AccountModel.getMoney(winner.getPlayer().getId()) + 200);
        AccountModel.setGamesPlayed(winnerId, AccountModel.getGamesPlayed(winnerId) + 1);
        AccountModel.setGamesWon(winnerId, AccountModel.getGamesWon(winnerId) + 1);

        AccountModel.setGamesPlayed(loserId, (AccountModel.getGamesPlayed(loserId) + 1));

        Asleac winnerAsleac = winner.getId() == battlePlayer1.getId() ? battleAsleac1 : battleAsleac2;
        Asleac loserAsleac = winner.getId() == battlePlayer1.getId() ? battleAsleac2 : battleAsleac1;

        AsleacModel.setLevel(winnerAsleac.getId(), calculateLevel(winnerAsleac, loserAsleac));

        BattleInfoPacket packet = new BattleInfoPacket(BattleInfoPacket.BattleState.END, winner.getPlayer().getUsername(), loser.getPlayer().getUsername());
        PacketManager.sendPacket(packet, winner.getChannel());
        PacketManager.sendPacket(packet, loser.getChannel());

        winner.update();
        loser.update();

        Logger.info(winner.getPlayer().getUsername() + " and " + loser.getPlayer().getUsername() + " finished their Battle!");
    }

    private double calculateLevel(Asleac winner, Asleac loser) {
        double currentLevel = AsleacModel.getLevel(winner.getId());
        double addition;
        if (winner.getLevel() > loser.getLevel())
            addition = 0.1;
        else
            addition = (1.0 + (loser.getLevel() - winner.getLevel())) / 10.0;
        currentLevel += addition;
        return currentLevel;
    }

}
