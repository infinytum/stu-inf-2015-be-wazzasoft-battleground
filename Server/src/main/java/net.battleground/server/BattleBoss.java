package net.battleground.server;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.battleground.Logger;
import net.battleground.Server;
import net.battleground.network.Packet;
import net.battleground.network.PacketManager;
import net.battleground.packet.HandshakePacket;

/**
 * Created by bteusm on 16.03.2016.
 */
public class BattleBoss extends SimpleChannelInboundHandler<Packet> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        try {
            super.channelActive(ctx);
            ServerPlayer player = new ServerPlayer(ctx.channel());
            Server.getPlayers().add(player);
            Logger.info("Starting Upstream Handler for incoming connection [" + player.getChannel().localAddress() + "]");

            HandshakePacket handshakePacket = new HandshakePacket(player.getId());
            PacketManager.sendPacket(handshakePacket, player.getChannel());
        } catch (Exception ignored) {

        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        try {
            PacketManager.handlePacket((Packet) msg);
            super.channelRead(ctx, msg);
        } catch (Exception ex) {

        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet s) {
    }
}
