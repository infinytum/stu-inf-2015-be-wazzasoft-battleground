package net.battleground.server;

import net.battleground.ability.Ability;
import net.battleground.entity.Asleac;
import net.battleground.entity.AsleacType;
import net.battleground.model.AbilityModel;

/**
 * Created by bteusm on 24.03.2016.
 */
public class ServerAbility {

    private Ability ability;

    public ServerAbility(int id) {
        ability = new Ability();
        String name = AbilityModel.getAbilityName(id);
        AsleacType type = AsleacType.valueOf(AbilityModel.getTypeName(id).toUpperCase());
        int strength = AbilityModel.getStrength(id);
        int mana = AbilityModel.getMaxEnergy(id);
        setName(name);
        setType(type);
        setStrength(strength);
        setMana(mana);
        setId(id);
    }

    public Ability getAbility() {
        return ability;
    }

    public boolean use(Asleac target, Asleac attacker) {
        return ability.use(target, attacker);
    }


    public int getId() {
        return ability.getId();
    }

    protected void setId(int id) {
        ability.setId(id);
    }

    public String getName() {
        return ability.getName();
    }

    public void setName(String name) {
        ability.setName(name);
    }

    public AsleacType getType() {
        return ability.getType();
    }

    protected void setType(AsleacType type) {
        ability.setType(type);
    }

    public int getStrength() {
        return ability.getStrength();
    }

    public void setStrength(int strength) {
        ability.setStrength(strength);
    }

    public int getMana() {
        return ability.getMana();
    }

    protected void setMana(int mana) {
        ability.setMana(mana);
    }
}
