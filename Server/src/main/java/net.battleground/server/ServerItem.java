package net.battleground.server;

import net.battleground.item.Item;
import net.battleground.model.ItemModel;

/**
 * Created by Michael on 28.03.2016.
 */
public class ServerItem {

    private Item item;

    public ServerItem(int item_id) {
        item = new Item();
        item.setId(item_id);
        item.setName(ItemModel.getItemName(item_id));
        item.setPrice(ItemModel.getItemPrice(item_id));
        item.setStrength(ItemModel.getItemStrength(item_id));
    }

    public Item getItem() {
        return item;
    }
}
