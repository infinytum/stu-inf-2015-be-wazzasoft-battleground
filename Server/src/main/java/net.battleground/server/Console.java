package net.battleground.server;

import java.util.Scanner;

/**
 * Created by bteusm on 24.02.2016.
 */
public class Console {

    public static void write(Object message) {
        System.out.print(message);
    }

    public static void writeLine(Object message) {
        System.out.println(message);
    }

    public static String read(String message) {
        Scanner scanner = new Scanner(System.in);

        write(message + " ");
        scanner.hasNext();
        return scanner.next();
    }

    public static String readLine(String message) {
        Scanner scanner = new Scanner(System.in);

        write(message);
        scanner.hasNext();
        return scanner.nextLine();
    }

    public static int readInt(String message) {
        Scanner scanner = new Scanner(System.in);

        write(message + " ");
        scanner.hasNext();
        return scanner.nextInt();
    }

    public static double readDouble(String message) {
        Scanner scanner = new Scanner(System.in);

        write(message + " ");
        scanner.hasNext();
        return scanner.nextDouble();
    }

    public static boolean readBoolean(String message) {
        Scanner scanner = new Scanner(System.in);

        write(message + " ");
        scanner.hasNext();
        return scanner.nextBoolean();
    }

    public static String read() {
        return read("");
    }

    public static String readLine() {
        return readLine("");
    }

    public static int readInt() {
        return readInt("");
    }

    public static double readDouble() {
        return readDouble("");
    }

    public static boolean readBoolean() {
        return readBoolean("");
    }
}
