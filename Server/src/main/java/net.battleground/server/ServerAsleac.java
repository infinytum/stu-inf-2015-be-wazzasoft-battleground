package net.battleground.server;

import net.battleground.ability.Ability;
import net.battleground.entity.Asleac;
import net.battleground.entity.AsleacType;
import net.battleground.model.AsleacModel;
import net.battleground.model.AsleacTypeModel;
import net.battleground.model.TypeModel;

import java.util.ArrayList;

/**
 * Created by bteusm on 24.03.2016.
 */
public class ServerAsleac {

    private Asleac asleac;

    public ServerAsleac(int id) {
        asleac = new Asleac();
        double level = AsleacModel.getLevel(id);
        ArrayList<Ability> abilityArrayList = new ArrayList<>();
        for (int i : AsleacModel.getAbilities(id))
            abilityArrayList.add(new ServerAbility(i).getAbility());
        int asleacRoots = AsleacModel.getElement(id);
        AsleacType type = AsleacType.valueOf(TypeModel.getTypeName(asleacRoots).toUpperCase());

        setId(id);
        setLevel(level);
        setName(AsleacTypeModel.getName(AsleacModel.getAsleacTypeID(id)));
        setMaxHealth((AsleacModel.getHealth(id)) * (int) getLevel());
        setType(type);
        setAbilities(abilityArrayList);
        setCustomName(AsleacModel.getCustomName(id));
        setHealth(getMaxHealth());
    }

    public Asleac getAsleac() {
        return asleac;
    }

    public boolean isAlive() {
        return asleac.isAlive();
    }


    public int getPrice() {
        return asleac.getPrice();
    }


    public int getId() {
        return asleac.getId();
    }

    public void setId(int id) {
        asleac.setId(id);
    }

    public String getName() {
        return asleac.getName();
    }

    public void setName(String name) {
        asleac.setName(name);
    }

    public AsleacType getType() {
        return asleac.getType();
    }

    public void setType(AsleacType type) {
        asleac.setType(type);
    }

    public int getHealth() {
        return asleac.getHealth();
    }

    public void setHealth(int health) {
        asleac.setHealth(health);
    }

    public int getMaxHealth() {
        return asleac.getMaxHealth();
    }

    public void setMaxHealth(int maxHealth) {
        asleac.setMaxHealth(maxHealth);
    }

    public double getLevel() {
        return asleac.getLevel();
    }

    public void setLevel(double level) {
        asleac.setLevel(level);
    }

    public ArrayList<Ability> getAbilities() {
        return asleac.getAbilities();
    }

    public void setAbilities(ArrayList<Ability> abilities) {
        asleac.setAbilities(abilities);
    }


    public String getCustomName() {
        return asleac.getCustomName();
    }


    public void setCustomName(String customName) {
        asleac.setCustomName(customName);
    }


    public void damage(Ability ability, Asleac attacker) {
        asleac.damage(ability, attacker);
    }
}
