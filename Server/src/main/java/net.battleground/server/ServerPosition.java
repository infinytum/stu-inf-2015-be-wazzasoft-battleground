package net.battleground.server;

import net.battleground.Position;
import net.battleground.entity.Player;
import net.battleground.item.Item;
import net.battleground.model.ItemAccountModel;

/**
 * Created by Michael on 28.03.2016.
 */
public class ServerPosition {
    private Position position;

    public ServerPosition(Item item, Player owner) {
        position = new Position(owner);
        position.setItem(item);
        position.setAmount(ItemAccountModel.getPossessedAmount(item.getId(), owner.getId()));
    }

    public Position getPosition() {
        return position;
    }

    public int getAmount() {
        return position.getAmount();
    }

    public void setAmount(int amount) {
        position.setAmount(amount);
        ItemAccountModel.setItemPossessed(position.getItem().getId(), position.getOwner().getId(), amount);
    }

}
