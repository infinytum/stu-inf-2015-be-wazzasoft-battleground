package net.battleground.server;


import io.netty.channel.Channel;
import net.battleground.Position;
import net.battleground.entity.Asleac;
import net.battleground.entity.Player;
import net.battleground.model.AccountModel;
import net.battleground.network.PacketManager;
import net.battleground.packet.PlayerUpdatePacket;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bteusm on 16.03.2016.
 */
public class ServerPlayer {

    private static int counter = 1;
    private static HashMap<Integer, ServerPlayer> playerMap = new HashMap<Integer, ServerPlayer>();

    private int id;
    private Channel channel;
    private boolean loggedIn = false;
    private Player player;


    public ServerPlayer(Channel channel) {
        id = counter++;
        playerMap.put(id, this);
        this.channel = channel;
    }

    public static ServerPlayer getChannelPlayer(int id) {
        if (playerMap.containsKey(id))
            return playerMap.get(id);
        return null;
    }

    public int getId() {
        return id;
    }

    public Channel getChannel() {
        return channel;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public void setUsername(String username) {
        player = new Player();
        player.setUsername(username);
        update();
    }

    public void update() {
        player.setId(AccountModel.getAccountID(getPlayer().getUsername()));
        int dbID = AccountModel.getAccountID(getPlayer().getUsername());
        player.setEmail(AccountModel.getEmail(dbID));
        ArrayList<Asleac> asleacs = new ArrayList<>();
        for (int i : AccountModel.getAsleacs(dbID)) {
            asleacs.add(new ServerAsleac(i).getAsleac());
        }
        ArrayList<Position> positions = new ArrayList<>();
        for (int i : AccountModel.getItems(dbID)) {
            positions.add(new ServerPosition(new ServerItem(i).getItem(), player).getPosition());
        }
        player.setItems(positions);
        player.setAsleacs(asleacs);
        PacketManager.sendPacket(new PlayerUpdatePacket(getPlayer()), getChannel());
    }

    public void logout() {
        setLoggedIn(false);
        playerMap.remove(id);
    }

    public Player getPlayer() {
        return player;
    }
}
