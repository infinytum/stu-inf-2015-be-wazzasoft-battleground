package net.battleground;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import net.battleground.command.CommandManager;
import net.battleground.config.ServerConfig;
import net.battleground.network.Packet;
import net.battleground.server.BattleBoss;
import net.battleground.server.Console;
import net.battleground.server.ServerPlayer;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by bteusm on 16.03.2016.
 */
public class Server {

    private static Collection<ServerPlayer> players = new ArrayList<>();
    private static boolean run = true;
    private static ChannelFuture f;
    private static ServerConfig config;

    public static void main(String[] args) throws Exception {
        new Server().run();
    }

    public static Collection<ServerPlayer> getPlayers() {
        return players;
    }

    public static ServerConfig getConfig() {
        return config;
    }

    public static void shutdown() {
        if (!run)
            return;

        Logger.info("Shutting down Battleground WFC Server...");
        for (ServerPlayer player : players) {
            player.getChannel().disconnect();
        }
        f.channel().close();
        run = false;
    }

    public void run() throws Exception {

        Logger.info("Starting up Battleground WFC Server...");
        config = new ServerConfig();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                shutdown();
            }
        });

        EventLoopGroup bossGroup = new NioEventLoopGroup(); // (1)
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            SelfSignedCertificate ssc = new SelfSignedCertificate();
            SslContext sslCtx = SslContext.newServerContext(ssc.certificate(), ssc.privateKey());
            ServerBootstrap b = new ServerBootstrap(); // (2)
            b.option(ChannelOption.SO_RCVBUF, 1024 * 1024 * 1024);
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class) // (3)
                    .childHandler(new ChannelInitializer<SocketChannel>() { // (4)
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast("encoder", new ObjectEncoder());
                            ch.pipeline().addLast("decoder", new ObjectDecoder(1024 * 1024 * 1024, ClassResolvers.softCachingResolver(Packet.class.getClassLoader())));
                            ch.pipeline().addLast(new BattleBoss());
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)          // (5)
                    .childOption(ChannelOption.SO_KEEPALIVE, true); // (6)

            // Bind and start to accept incoming connections.
            f = b.bind(43110); // (7)

            while (run) {
                String line = Console.readLine();
                String[] splittedLine = line.split(" ");
                String command = splittedLine[0];
                ArrayList<String> argsList = new ArrayList<>();
                for (int i = 1; i < splittedLine.length; i++) {
                    argsList.add(splittedLine[i]);
                }

                CommandManager.executeCommand(command, argsList.toArray(new String[0]));

            }

            // Wait until the server socket is closed.
            // In this example, this does not happen, but you can do that to gracefully
            // shut down your server.
            f.channel().closeFuture().sync();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
}
