package net.battleground.listener;

import net.battleground.Position;
import net.battleground.entity.Asleac;
import net.battleground.model.*;
import net.battleground.network.PacketManager;
import net.battleground.packet.*;
import net.battleground.server.ServerAsleac;
import net.battleground.server.ServerItem;
import net.battleground.server.ServerPlayer;
import net.battleground.server.ServerPosition;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Michael on 28.03.2016.
 */
public class ShopListener implements Listener {

    @PacketHandler
    public void onShopRequestReceive(ShopRequestPacket packet) {
        ServerPlayer player = ServerPlayer.getChannelPlayer(packet.getSender());
        int player_id = AccountModel.getAccountID(player.getPlayer().getUsername());
        ArrayList<Asleac> ownedAsleacs = new ArrayList<>();
        ArrayList<Position> item_positions = new ArrayList<>();
        ArrayList<Asleac> buyableAsleacs = new ArrayList<>();

        for (int asleac_id : AccountModel.getAsleacs(player_id)) {
            ServerAsleac serverAsleac = new ServerAsleac(asleac_id);
            ownedAsleacs.add(serverAsleac.getAsleac());
        }

        for (int asleacType_id : AsleacTypeModel.getAsleacTypes()) {
            Asleac buyAsleac = new Asleac();
            buyAsleac.setId(asleacType_id);
            buyAsleac.setName(AsleacTypeModel.getName(asleacType_id));
            buyAsleac.setLevel(5);
            buyableAsleacs.add(buyAsleac);
        }

        for (int item_id : ItemModel.getAllItems()) {
            ServerItem serverItem = new ServerItem(item_id);
            ServerPosition serverPosition = new ServerPosition(serverItem.getItem(), player.getPlayer());
            item_positions.add(serverPosition.getPosition());
        }
        ShopResponsePacket shopResponsePacket = new ShopResponsePacket(AccountModel.getMoney(player_id), ownedAsleacs, buyableAsleacs, item_positions);

        PacketManager.sendPacket(shopResponsePacket, player.getChannel());

        player.update();
    }

    @PacketHandler
    public void onBuyItemRequestReceive(BuyItemRequestPacket packet) {
        ServerPlayer player = ServerPlayer.getChannelPlayer(packet.getSender());
        int player_id = AccountModel.getAccountID(player.getPlayer().getUsername());

        if (AccountModel.getMoney(player_id) >= ItemModel.getItemPrice(packet.getItem_id())) {
            ItemAccountModel.setItemPossessed(packet.getItem_id(), player_id, ItemAccountModel.getPossessedAmount(packet.getItem_id(), player_id) + 1);
            AccountModel.setMoney(player_id, AccountModel.getMoney(player_id) - ItemModel.getItemPrice(packet.getItem_id()));
        }
        player.update();
    }

    @PacketHandler
    public void onBuyAsleacPacketReceive(BuyAsleacPacket packet) {
        ServerPlayer player = ServerPlayer.getChannelPlayer(packet.getSender());
        int player_id = AccountModel.getAccountID(player.getPlayer().getUsername());
        int count = AccountModel.countAsleacPossessed(player_id);
        int money = AccountModel.getMoney(player_id);
        int price = 100;

        if (count < 6) {
            if (count == 0) {
                Random random = new Random();
                int length = AsleacTypeModel.getAsleacTypes().size();
                AsleacModel.createAsleac(packet.getCustomName(), random.nextInt(length - 1) + 1, player_id);
                AccountModel.setAsleacsPossessed(player_id, AccountModel.getAsleacsPossessed(player_id) + 1);
                int asleac = AccountModel.getAsleacs(player_id).get(count);
                giveAsleacAbilities(asleac);

            } else if (count > 0 && money > price) {
                int asleacTypeID = AsleacTypeModel.getID(packet.getAsleac().getName());//works because a Asleac got submitted for buying a new ASleac
                AsleacModel.createAsleac(packet.getCustomName(), asleacTypeID, player_id);
                AccountModel.setMoney(player_id, money - price);
                AccountModel.setAsleacsPossessed(player_id, AccountModel.getAsleacsPossessed(player_id) + 1);
                int asleac = AccountModel.getAsleacs(player_id).get(count);
                giveAsleacAbilities(asleac);
            }
        }
        player.update();
    }

    private void giveAsleacAbilities(int asleac) {
        ArrayList<Integer> normal = AbilityModel.getAllAbilitiesWithType(TypeModel.getTypeID("Normal"));
        AsleacModel.giveAsleacAbility(asleac, normal.get(0));
        AsleacModel.giveAsleacAbility(asleac, normal.get(1));

        ArrayList<Integer> abilities = AbilityModel.getAllAbilitiesWithType(AsleacTypeModel.getType(AsleacModel.getAsleacTypeID(asleac)));
        AsleacModel.giveAsleacAbility(asleac, abilities.get(0));
        AsleacModel.giveAsleacAbility(asleac, abilities.get(1));
    }

    @PacketHandler
    public void onSellAsleacPacketReceive(SellAsleacPacket packet) {
        ServerPlayer player = ServerPlayer.getChannelPlayer(packet.getSender());
        int player_id = AccountModel.getAccountID(player.getPlayer().getUsername());
        int count = AccountModel.countAsleacPossessed(player_id);
        if (count > 1) {
            ServerAsleac serverAsleac = new ServerAsleac(packet.getAsleacID());
            int addMoney = serverAsleac.getAsleac().getPrice();
            AccountModel.setMoney(player_id, AccountModel.getMoney(player_id) + addMoney);
            AsleacModel.deleteAsleac(packet.getAsleacID());
        }
        player.update();
    }
}
