package net.battleground.listener;

import net.battleground.Logger;
import net.battleground.model.AccountModel;
import net.battleground.network.PacketManager;
import net.battleground.packet.LoginPacket;
import net.battleground.packet.LoginStatusPacket;
import net.battleground.server.ServerPlayer;
import org.jasypt.util.password.StrongPasswordEncryptor;

/**
 * Created by bteusm on 16.03.2016.
 */
public class LoginListener implements Listener {

    @PacketHandler
    public void onLoginReceive(LoginPacket t) {
        ServerPlayer p = ServerPlayer.getChannelPlayer(t.getSender());
        LoginStatusPacket packet;
        if (!AccountModel.doesUsernameExist(t.getUsername())) {
            packet = new LoginStatusPacket(false);
        } else {
            int id = AccountModel.getAccountID(t.getUsername());
            StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
            if (encryptor.checkPassword(t.getPassword(), AccountModel.getPassword(id))) {
                p.setUsername(t.getUsername());
                p.setLoggedIn(true);
                packet = new LoginStatusPacket(true, p.getPlayer());
                Logger.info(t.getUsername() + " has succeed authentication");
            } else {
                packet = new LoginStatusPacket(false);
            }
        }
        PacketManager.sendPacket(packet, p.getChannel());
        if (!packet.isSuccess())
            Logger.info(t.getUsername() + " has failed authentication");
    }

}
