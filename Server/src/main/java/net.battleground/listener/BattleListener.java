package net.battleground.listener;

import net.battleground.controller.BattleController;
import net.battleground.network.PacketManager;
import net.battleground.packet.BattleAttackPacket;
import net.battleground.packet.BattleInfoPacket;
import net.battleground.packet.BattleItemPacket;
import net.battleground.packet.PlayerUpdatePacket;
import net.battleground.server.ServerPlayer;
import net.battleground.server.ServerPosition;

/**
 * Created by bteusm on 24.03.2016.
 */
public class BattleListener implements Listener {

    @PacketHandler
    public void onBattleInfoPacketReceive(BattleInfoPacket packet) {
        ServerPlayer player = ServerPlayer.getChannelPlayer(packet.getSender());

        PacketManager.sendPacket(new PlayerUpdatePacket(player.getPlayer()), player.getChannel());

        if (!player.isLoggedIn())
            return;
        if (packet.getBattleState() == BattleInfoPacket.BattleState.WAITING) {
            if (!BattleController.isWaiting(player))
                BattleController.setWaiting(player);
        }
        if (packet.getBattleState() == BattleInfoPacket.BattleState.QUIT_WAIT) {
            BattleController.quitWaiting(player);
            PacketManager.sendPacket(new BattleInfoPacket(BattleInfoPacket.BattleState.QUIT_WAIT), player.getChannel());
        }
        if (packet.getBattleState() == BattleInfoPacket.BattleState.SURRENDER || packet.getBattleState() == BattleInfoPacket.BattleState.DIE) {
            BattleController controller = BattleController.getRunningBattle(player);
            if (controller == null)
                return;
            controller.lose(player);
        }
    }

    @PacketHandler
    public void onBattleAttackPacketReceive(BattleAttackPacket packet) {
        ServerPlayer attacker = ServerPlayer.getChannelPlayer(packet.getSender());
        if (!attacker.isLoggedIn())
            return;

        BattleController controller = BattleController.getRunningBattle(attacker);
        if (controller == null)
            return;

        ServerPlayer target = controller.getTargetPlayer(attacker);

        packet.setSender(0);

        PacketManager.sendPacket(packet, target.getChannel());
    }

    @PacketHandler
    public void onBattleItemPacketReceive(BattleItemPacket packet) {
        ServerPlayer itemUser = ServerPlayer.getChannelPlayer(packet.getSender());
        if (!itemUser.isLoggedIn())
            return;

        BattleController controller = BattleController.getRunningBattle(itemUser);
        if (controller == null)
            return;

        ServerPlayer target = controller.getTargetPlayer(itemUser);

        packet.setSender(0);

        ServerPosition position = new ServerPosition(packet.getItem(), itemUser.getPlayer());
        position.setAmount(position.getAmount() - 1);

        PacketManager.sendPacket(packet, target.getChannel());
    }

}
