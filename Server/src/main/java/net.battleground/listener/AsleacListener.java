package net.battleground.listener;

import net.battleground.entity.Asleac;
import net.battleground.model.AccountModel;
import net.battleground.network.PacketManager;
import net.battleground.packet.AsleacRequestPacket;
import net.battleground.packet.AsleacResponsePacket;
import net.battleground.server.ServerAsleac;
import net.battleground.server.ServerPlayer;

import java.util.ArrayList;

/**
 * Created by mkteu on 28.03.2016.
 */
public class AsleacListener implements Listener {

    @PacketHandler
    public void onAsleacRequestPacketReceive(AsleacRequestPacket packet) {
        ServerPlayer player = ServerPlayer.getChannelPlayer(packet.getSender());
        if (!player.isLoggedIn())
            return;

        ArrayList<Asleac> asleacs = new ArrayList<>();

        for (int i : AccountModel.getAsleacs(player.getPlayer().getId())) {
            asleacs.add(new ServerAsleac(i).getAsleac());
        }

        PacketManager.sendPacket(new AsleacResponsePacket(asleacs), player.getChannel());
    }

}
