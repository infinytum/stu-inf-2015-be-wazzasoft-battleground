package net.battleground.listener;

import net.battleground.Logger;
import net.battleground.model.AccountModel;
import net.battleground.network.PacketManager;
import net.battleground.packet.RegisterPacket;
import net.battleground.packet.RegisterStatusPacket;
import net.battleground.server.ServerPlayer;
import org.apache.commons.validator.routines.EmailValidator;
import org.jasypt.util.password.StrongPasswordEncryptor;

/**
 * Created by bteusm on 23.03.2016.
 */
public class RegisterListener implements Listener {

    @PacketHandler
    public void onRegisterPacketReceive(RegisterPacket packet) {
        RegisterStatusPacket response;
        if (packet.getPassword().length() < 8)
            response = new RegisterStatusPacket(false, "Password is too short! At least 8 chars!");
        else if (!packet.getPassword_rep().equals(packet.getPassword()))
            response = new RegisterStatusPacket(false, "Passwords do not match!");
        else if (!EmailValidator.getInstance().isValid(packet.getEmail()))
            response = new RegisterStatusPacket(false, "Email is not valid");
        else if (AccountModel.doesEmailExist(packet.getEmail()))
            response = new RegisterStatusPacket(false, "Email already exists");
        else if (packet.getUsername().length() < 3)
            response = new RegisterStatusPacket(false, "Username is too short! At least 3 chars!");
        else if (packet.getUsername().length() > 16)
            response = new RegisterStatusPacket(false, "Username is too long! Only 16 chars!");
        else if (AccountModel.doesUsernameExist(packet.getUsername()))
            response = new RegisterStatusPacket(false, "Username already exists!");
        else {
            StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
            AccountModel.register(packet.getUsername(), packet.getEmail(), passwordEncryptor.encryptPassword(packet.getPassword()));

            response = new RegisterStatusPacket(true, "");
            Logger.info(packet.getUsername() + " successfully registered at Battleground!");
        }
        PacketManager.sendPacket(response, ServerPlayer.getChannelPlayer(packet.getSender()).getChannel());
    }

}
