package net.battleground.listener;

import net.battleground.Logger;
import net.battleground.Server;
import net.battleground.controller.BattleController;
import net.battleground.packet.LogoutPacket;
import net.battleground.server.ServerPlayer;

/**
 * Created by bteusm on 18.03.2016.
 */
public class ChannelListener implements Listener {

    @PacketHandler
    public void onLogoutPacketReceive(LogoutPacket packet) {
        ServerPlayer p = ServerPlayer.getChannelPlayer(packet.getSender());
        if (p.isLoggedIn()) {
            Logger.info(p.getPlayer().getUsername() + " has disconnected");
            if (BattleController.getRunningBattle(p) != null)
                BattleController.getRunningBattle(p).lose(p);
            else
                BattleController.quitWaiting(p);
        } else
            Logger.info("Upstream Handler lost connection [" + p.getChannel().localAddress() + "]");
        Server.getPlayers().remove(p);
        p.logout();
    }

}
