package net.battleground.listener;

import net.battleground.model.AccountModel;
import net.battleground.network.PacketManager;
import net.battleground.packet.StatisticRequestPacket;
import net.battleground.packet.StatisticResponsePacket;
import net.battleground.server.ServerPlayer;

/**
 * Created by bteusm on 23.03.2016.
 */
public class StatisticListener implements Listener {

    //TODO: Send real stats from database
    @PacketHandler
    public void onStatisticRequestReceive(StatisticRequestPacket packet) {
        ServerPlayer player = ServerPlayer.getChannelPlayer(packet.getSender());

        int id = AccountModel.getAccountID(player.getPlayer().getUsername());
        StatisticResponsePacket statisticResponsePacket = new StatisticResponsePacket(AccountModel.getGamesWon(id), AccountModel.getGamesPlayed(id), AccountModel.getMoney(id), AccountModel.getAsleacsPossessed(id));
        PacketManager.sendPacket(statisticResponsePacket, player.getChannel());
    }

}
