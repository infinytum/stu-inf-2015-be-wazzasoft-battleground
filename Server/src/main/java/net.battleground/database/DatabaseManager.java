package net.battleground.database;

import net.battleground.Logger;
import net.battleground.Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by bteusm on 23.03.2016.
 */
public class DatabaseManager {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static Connection con;
    private static String DB_URL = "jdbc:mysql://" + Server.getConfig().getDatabaseHost() + "/asleac?autoReconnect=true";

    private DatabaseManager() {
    }

    public static Connection getConnection() {
        try {
            if (con == null || con.isClosed()) {
                Logger.info("Connecting to Database...");
                Class.forName(JDBC_DRIVER);
                DB_URL = "jdbc:mysql://" + Server.getConfig().getDatabaseHost() + "/asleac?autoReconnect=true";
                con = DriverManager.getConnection(DB_URL, Server.getConfig().getDatabaseUsername(), Server.getConfig().getDatabasePassword());
            }
            return con;
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
