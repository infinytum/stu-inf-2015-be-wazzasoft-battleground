package net.battleground.command;

import net.battleground.Server;

/**
 * Created by bteusm on 30.03.2016.
 */
public class ExitCommand extends Command {

    public ExitCommand() {
        super(Commands.EXIT_COMMAND);
    }

    @Override
    public void onExecute(String command, String alias, String[] args) {
        Server.shutdown();
    }
}
