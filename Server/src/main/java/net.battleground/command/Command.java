package net.battleground.command;

/**
 * Created by bteusm on 30.03.2016.
 */
public abstract class Command {

    private String command;
    private String[] aliases;

    protected Command(Commands commands) {
        this.command = commands.getCommandName();
        this.aliases = commands.getCommandAliases();
    }

    public abstract void onExecute(String command, String alias, String[] args);

}
