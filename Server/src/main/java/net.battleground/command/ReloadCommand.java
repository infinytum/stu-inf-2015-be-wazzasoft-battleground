package net.battleground.command;

import net.battleground.Logger;
import net.battleground.Server;
import net.battleground.database.DatabaseManager;

import java.sql.SQLException;

/**
 * Created by bteusm on 30.03.2016.
 */
public class ReloadCommand extends Command {

    public ReloadCommand() {
        super(Commands.EXIT_COMMAND);
    }

    @Override
    public void onExecute(String command, String alias, String[] args) {
        Logger.info("Reloading Server configuration...");
        Server.getConfig().load();
        try {
            if (DatabaseManager.getConnection() != null)
                DatabaseManager.getConnection().close();
        } catch (SQLException e) {

        }
        Server.getConfig().load();
        Logger.info("Reload complete!");
    }
}
