package net.battleground.command;

/**
 * Created by bteusm on 30.03.2016.
 */
public enum Commands {

    EXIT_COMMAND {
        @Override
        public Command getCommand() {
            return new ExitCommand();
        }

        @Override
        public String getCommandName() {
            return "exit";
        }

        @Override
        public String[] getCommandAliases() {
            return new String[]{"stop", "end", "shutdown"};
        }
    }, RELOAD_COMMAND {
        @Override
        public Command getCommand() {
            return new ReloadCommand();
        }

        @Override
        public String getCommandName() {
            return "reload";
        }

        @Override
        public String[] getCommandAliases() {
            return new String[]{"rl"};
        }
    };


    public static Commands getCommandType(Command command) {
        for (Commands cmd : values()) {
            if (command.getClass() == cmd.getCommand().getClass())
                return cmd;
        }
        return null;
    }

    public static Commands getCommandByName(String command) {
        for (Commands cmd : values()) {
            if (cmd.getCommandName().equalsIgnoreCase(command))
                return cmd;
            else if (isAliasOf(cmd, command))
                return cmd;
        }
        return null;
    }

    public static boolean isAliasOf(Commands cmd, String alias) {
        for (String a : cmd.getCommandAliases()) {
            if (a.equalsIgnoreCase(alias))
                return true;
        }
        return false;
    }


    public abstract String getCommandName();

    public abstract Command getCommand();

    public abstract String[] getCommandAliases();
}
