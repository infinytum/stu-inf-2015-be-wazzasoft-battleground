package net.battleground.command;

/**
 * Created by bteusm on 30.03.2016.
 */
public abstract class CommandManager {


    public static void executeCommand(String name, String[] args) {
        Commands cmd = Commands.getCommandByName(name);
        if (cmd != null) {
            cmd.getCommand().onExecute(cmd.getCommandName(), name, args);
        }
    }

}
