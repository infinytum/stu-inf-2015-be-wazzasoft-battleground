package net.battleground.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bwaltm on 24.03.2016.
 */
public class AbilityModel extends Model {

    public static ArrayList<Integer> getAllAbilities() {

        ArrayList<Integer> abilities = new ArrayList<>();
        try {
            ArrayList<Object> params = new ArrayList<>();
            ResultSet rs = select("Ability",
                    "id", "1=1", params);

            while (rs.next()) {
                abilities.add(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return abilities;
    }

    public static String getAbilityName(int id) {
        String result = "fail";

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Ability", "name", "id=?", params);

            while (rs.next()) {
                result = rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getAbilityID(String name) {
        int result = 0;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(name);
            ResultSet rs = select("Ability", "id", "name=?", params);

            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static int getStat(String column, int id) {
        int result = 0;
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Ability", column, "id=?", params);
            while (rs.next()) {
                result = rs.getInt(column);
            }
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getMaxEnergy(int id) {
        return getStat("maxEnergy", id);
    }

    public static int getStrength(int id) {
        return getStat("strength", id);
    }

    public static String getTypeName(int id) {
        String result = "fail";
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Ability as a JOIN Type as t on a.type_id=t.id", "t.name", "a.id=?", params);
            while (rs.next()) {
                result = rs.getString("t.name");
            }
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ArrayList<Integer> getAllAbilitiesWithType(int type_id) {
        ArrayList<Integer> abilities = new ArrayList<>();
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(type_id);
            ResultSet rs = select("Ability",
                    "id", "type_id=?", params);

            while (rs.next()) {
                abilities.add(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return abilities;
    }
}
