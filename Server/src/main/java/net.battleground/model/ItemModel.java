package net.battleground.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bwaltm on 24.03.2016.
 */
public class ItemModel extends Model {
    public static ArrayList<Integer> getAllItems() {
        ArrayList<Integer> asleacTypes = new ArrayList<>();
        try {
            ArrayList<Object> params = new ArrayList<>();
            ResultSet rs = select("Item",
                    "id", "1=1", params);

            while (rs.next()) {
                asleacTypes.add(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return asleacTypes;
    }

    public static int getItemID(String name) {
        int result = 0;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(name);
            ResultSet rs = select("Item", "id", "name=?", params);

            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getItemName(int id) {
        String result = "fail";

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Item", "name", "id=?", params);

            while (rs.next()) {
                result = rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getItemPrice(int id) {
        int result = 0;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Item", "price", "id=?", params);

            while (rs.next()) {
                result = rs.getInt("price");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getItemStrength(int id) {
        int result = 0;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Item", "strength", "id=?", params);

            while (rs.next()) {
                result = rs.getInt("strength");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
