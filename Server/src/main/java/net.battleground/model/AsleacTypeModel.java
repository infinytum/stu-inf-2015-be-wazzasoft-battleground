package net.battleground.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bwaltm on 24.03.2016.
 */
public class AsleacTypeModel extends Model {
    public static ArrayList<Integer> getAsleacTypes() {
        ArrayList<Integer> asleacTypes = new ArrayList<>();
        try {
            ArrayList<Object> params = new ArrayList<>();
            ResultSet rs = select("AsleacType",
                    "id", "1=1", params);

            while (rs.next()) {
                asleacTypes.add(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return asleacTypes;
    }

    public static int getID(String name) {
        int result = 0;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(name);
            ResultSet rs = select("AsleacType", "id", "name=?", params);

            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getName(int id) {
        String result = "fail";

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("AsleacType", "name", "id=?", params);

            while (rs.next()) {
                result = rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getType(int id) {
        int result = 0;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("AsleacType", "type_id", "id=?", params);

            while (rs.next()) {
                result = rs.getInt("type_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
