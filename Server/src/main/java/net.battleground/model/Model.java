package net.battleground.model;

import net.battleground.database.DatabaseManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bwaltm on 23.03.2016.
 */
public abstract class Model {
    protected static ResultSet select(String tablename, String what, String conditions, ArrayList<Object> params) {
        String statement = "SELECT " + what + " FROM " + tablename + " WHERE " + conditions;
        try {
            PreparedStatement preparedStatement = DatabaseManager.getConnection().prepareStatement(statement);
            int i = 1;
            for (Object param : params) {
                if (param instanceof String) {
                    preparedStatement.setString(i, (String) param);
                } else if (param instanceof Integer) {
                    preparedStatement.setInt(i, (Integer) param);
                } else if (param instanceof Double) {
                    preparedStatement.setDouble(i, (Double) param);
                }
                ++i;
            }
            preparedStatement.execute();
            return preparedStatement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static boolean exists(String tablename, String what, String conditions, ArrayList<Object> params) {
        boolean existing = false;
        try {
            ResultSet rs = select(tablename, what, conditions, params);

            while (rs.next()) {
                existing = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existing;
    }

    protected static void delete(String tablename, String conditions, ArrayList<Object> params) {
        String statement = "DELETE FROM " + tablename + " WHERE " + conditions;
        executeStatement(statement, params);
    }

    protected static void update(String tablename, String what, String conditions, ArrayList<Object> params) {
        String statement = "UPDATE " + tablename + " SET " + what + " = ? WHERE " + conditions;
        executeStatement(statement, params);
    }

    protected static void insert(String tablename, String columns, String values, ArrayList<Object> params) {
        String statement = "INSERT INTO " + tablename + " ( " + columns + " ) VALUES ( " + values + " )";
        executeStatement(statement, params);
    }

    protected static void executeStatement(String statement, ArrayList<Object> params) {
        int i = 1;
        try {
            PreparedStatement preparedStatement = DatabaseManager.getConnection().prepareStatement(statement);
            for (Object param : params) {
                if (param instanceof String) {
                    preparedStatement.setString(i, (String) param);
                } else if (param instanceof Integer) {
                    preparedStatement.setInt(i, (Integer) param);
                } else if (param instanceof Double) {
                    preparedStatement.setDouble(i, (Double) param);
                }
                ++i;
            }
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
