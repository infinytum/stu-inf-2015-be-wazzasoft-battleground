package net.battleground.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bwaltm on 24.03.2016.
 */
public class AsleacModel extends Model {

    public static void createAsleac(String customName, int asleacTypeID, int accountID) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(customName);
        params.add(asleacTypeID);
        params.add(accountID);
        insert("Asleac", "customName,asleacType_id,account_id", "?,?,?", params);
    }

    public static int getHealth(int id) {
        int result = 0;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Asleac AS a JOIN AsleacType AS at on at.id=a.asleacType_id", "health", "a.id=?", params);
            while (rs.next()) {
                result = rs.getInt("health");
            }
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static double getLevel(int id) {
        double result = 0.0;
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Asleac", "level", "id=?", params);
            while (rs.next()) {
                result = rs.getDouble("level");
            }
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getCustomName(int id) {
        String result = "fail";

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Asleac", "customName", "id=?", params);

            while (rs.next()) {
                result = rs.getString("customName");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getAsleacTypeID(int id) {
        int result = -1;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Asleac", "asleacType_id", "id=?", params);

            while (rs.next()) {
                result = rs.getInt("asleacType_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void setLevel(int id, double level) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(level);
        params.add(id);
        update("Asleac", "level", "id=?", params);
    }

    public static ArrayList<Integer> getAbilities(int id) {
        ArrayList<Integer> abilities = new ArrayList<>();
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Asleac AS a JOIN Asleac_Ability AS aa ON aa.asleac_id=a.id JOIN Ability AS ab ON ab.id=aa.ability_id",
                    "ab.id", "a.id=?", params);

            while (rs.next()) {
                abilities.add(rs.getInt("ab.id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return abilities;
    }

    public static int getElement(int id) {
        int element = 0;
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Asleac AS a JOIN AsleacType AS at ON at.id=a.asleacType_id JOIN Type as t on at.type_id = t.id",
                    "t.id", "a.id=?", params);

            while (rs.next()) {
                element = rs.getInt("t.id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return element;
    }

    public static void giveAsleacAbility(int asleacID, int abilityID) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(asleacID);
        params.add(abilityID);
        insert("Asleac_Ability", "asleac_id,ability_id", "?,?", params);
    }

    public static void deleteAsleac(int asleacID) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(asleacID);
        delete("Asleac", "id=?", params);
    }


}
