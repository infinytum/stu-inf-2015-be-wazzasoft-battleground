package net.battleground.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bwaltm on 23.03.2016.
 */
public class AccountModel extends Model {

    public static int getAccountID(String name) {
        int id = 0;
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(name);

            ResultSet rs = select("Account", "id", "username=?", params);

            while (rs.next()) {
                id = rs.getInt("id");
            }
            return id;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public static String getUsername(int id) {
        String username = "fail";
        try {
            ArrayList<Object> params = new ArrayList();
            params.add(id);
            ResultSet rs = select("Account", "username", "id=?", params);

            while (rs.next()) {
                username = rs.getString("username");
            }
            return username;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return username;
    }

    public static String getEmail(int id) {
        String email = "fail";
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Account", "email", "id=?", params);

            while (rs.next()) {
                email = rs.getString("email");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return email;
    }

    public static String getPassword(int id) {
        String password = "fail";
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Account", "password", "id=?", params);

            while (rs.next()) {
                password = rs.getString("password");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return password;
    }

    public static void register(String username, String email, String password) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(username);
        params.add(email);
        params.add(password);
        insert("Account", "username,email,password", "?,?,?", params);

    }

    public static int getMoney(int id) {
        return getStat("money", id);
    }

    public static int getGamesPlayed(int id) {
        return getStat("gamesPlayed", id);
    }

    public static int getGamesWon(int id) {
        return getStat("gamesWon", id);
    }

    public static int getAsleacsPossessed(int id) {
        return getStat("asleacsPossessed", id);
    }

    public static ArrayList<Integer> getAsleacs(int id) {
        ArrayList<Integer> result = new ArrayList<>();
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Asleac", "id", "account_id=?", params);

            while (rs.next()) {
                result.add(rs.getInt("id"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ArrayList<Integer> getItems(int id) {
        ArrayList<Integer> result = new ArrayList<>();
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("account_item", "item_id", "account_id=?", params);

            while (rs.next()) {
                result.add(rs.getInt("item_id"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static int getStat(String column, int id) {
        int result = 0;
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Account", column, "id=?", params);
            while (rs.next()) {
                result = rs.getInt(column);
            }
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void setMoney(int id, int money) {
        setStat("money", id, money);
    }

    public static void setGamesPlayed(int id, int gamesPlayed) {
        setStat("gamesPlayed", id, gamesPlayed);
    }

    public static void setGamesWon(int id, int gamesWon) {
        setStat("gamesWon", id, gamesWon);
    }

    public static void setAsleacsPossessed(int id, int asleacsPossessed) {
        setStat("asleacsPossessed", id, asleacsPossessed);
    }

    private static void setStat(String column, int id, int amount) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(amount);
        params.add(id);
        update("Account", column, "id=?", params);
    }

    public static boolean doesUsernameExist(String username) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(username);
        return exists("Account", "id", "username=?", params);

    }

    public static boolean doesEmailExist(String email) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(email);
        return exists("Account", "id", "email=?", params);
    }

    public static int countAsleacPossessed(int id) {
        int result = 0;
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Asleac", "COUNT(*) AS anzahl", "account_id=?", params);
            while (rs.next()) {
                result = rs.getInt("anzahl");
            }
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


}
