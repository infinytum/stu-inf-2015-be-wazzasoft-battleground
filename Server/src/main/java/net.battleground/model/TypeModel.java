package net.battleground.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bwaltm on 24.03.2016.
 */
public class TypeModel extends Model {


    public static int getTypeID(String name) {
        int result = 0;

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(name);
            ResultSet rs = select("Type", "id", "name=?", params);

            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String getTypeName(int id) {
        String result = "fail";

        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(id);
            ResultSet rs = select("Type", "name", "id=?", params);

            while (rs.next()) {
                result = rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }
}
