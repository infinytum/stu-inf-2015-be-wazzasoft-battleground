package net.battleground.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by bwaltm on 24.03.2016.
 */
public class ItemAccountModel extends Model {

    public static boolean doesUserPossess(int itemID, int accountID) {
        ArrayList<Object> params = new ArrayList<>();
        params.add(itemID);
        params.add(accountID);
        return exists("Account_Item", "amount", "item_id=? AND account_id=?", params);
    }

    public static int getPossessedAmount(int itemID, int accountID) {
        int result = 0;
        try {
            ArrayList<Object> params = new ArrayList<>();
            params.add(itemID);
            params.add(accountID);
            ResultSet rs = select("Account_Item", "amount", "item_id=? AND account_id=?", params);
            while (rs.next()) {
                result = rs.getInt("amount");
            }
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void setItemPossessed(int itemID, int accountID, int amount) {
        ArrayList<Object> params = new ArrayList<>();
        if (amount == 0) {
            params.add(itemID);
            params.add(accountID);
            delete("Account_Item", "item_id=? AND account_id=?", params);
        } else {
            params.add(amount);
            params.add(itemID);
            params.add(accountID);
            if (doesUserPossess(itemID, accountID)) {
                update("Account_Item", "amount", "item_id=? AND account_id=?", params);
            } else {
                insert("Account_Item", "amount,item_id,account_id", "?,?,?", params);
            }
        }
    }


}
