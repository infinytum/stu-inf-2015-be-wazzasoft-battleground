package net.battleground.item;

import net.battleground.entity.Asleac;

import java.awt.*;
import java.io.Serializable;

/**
 * Created by bteusm on 11.03.2016.
 */
public class Item implements Serializable {

    private int id;
    private String name;
    private int price;
    private int strength;
    private Image icon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public void use(Asleac asleac) {
        if ((asleac.getHealth() + getStrength()) >= asleac.getMaxHealth()) {
            asleac.setHealth(asleac.getMaxHealth());
            return;
        }
        asleac.setHealth((asleac.getHealth() + getStrength()));
    }
}
