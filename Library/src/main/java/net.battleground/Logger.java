package net.battleground;

/**
 * Created by Michael on 17.03.2016.
 */
public abstract class Logger {

    public static void info(String message) {
        System.out.println("[" + LogLevel.INFO + "] " + message);
    }

    public static void error(String message) {
        System.out.println("[" + LogLevel.ERROR + "] " + message);
    }

}
