package net.battleground.ability;

import net.battleground.entity.Asleac;
import net.battleground.entity.AsleacType;

import java.io.Serializable;

/**
 * Created by bteusm on 11.03.2016.
 */
public class Ability implements Serializable {

    private int id;
    private String name;
    private AsleacType type;
    private int strength;
    private int mana = 100;

    public Ability() {
    }

    public Ability(int id, String name, AsleacType type, int strength, int mana) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.strength = strength;
        this.mana = mana;
    }

    public boolean use(Asleac target, Asleac attacker) {
        if (mana == 0) {
            return false;
        }
        target.damage(this, attacker);
        --mana;
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    //TODO: REMOVE
    public void setName(String name) {
        this.name = name;
    }

    public AsleacType getType() {
        return type;
    }

    public void setType(AsleacType type) {
        this.type = type;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }
}
