package net.battleground;

import net.battleground.entity.Player;
import net.battleground.item.Item;

import java.io.Serializable;

/**
 * Created by bteusm on 11.03.2016.
 */
public class Position implements Serializable {

    private Item item;
    private int amount;
    private Player owner;

    public Position(Player owner) {
        this.owner = owner;
    }


    public int getPrice() {
        return (item.getPrice() * amount);
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }
}
