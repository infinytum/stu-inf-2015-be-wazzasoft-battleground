package net.battleground.entity;

import java.io.Serializable;

/*
 * Created by bteusm on 11.03.2016.
 */
public enum AsleacType implements Serializable {
    FIRE {
        public AsleacType[] getEffectiveAttackers() {
            return new AsleacType[]{WATER, GROUND};
        }
    },
    WATER {
        public AsleacType[] getEffectiveAttackers() {
            return new AsleacType[]{ELECTRIC, GRASS};
        }
    },
    GRASS {
        public AsleacType[] getEffectiveAttackers() {
            return new AsleacType[]{FIRE, ICE};
        }
    },
    ELECTRIC {
        public AsleacType[] getEffectiveAttackers() {
            return new AsleacType[]{GROUND};
        }
    },
    ICE {
        public AsleacType[] getEffectiveAttackers() {
            return new AsleacType[]{FIRE, FIGHT};
        }
    },
    GROUND {
        public AsleacType[] getEffectiveAttackers() {
            return new AsleacType[]{WATER, GRASS, ICE};
        }
    },
    FIGHT {
        public AsleacType[] getEffectiveAttackers() {
            return new AsleacType[]{ELECTRIC,};
        }
    },
    NORMAL {
        public AsleacType[] getEffectiveAttackers() {
            return new AsleacType[]{};
        }
    };


    public static boolean isEffectiveAgainst(AsleacType attacker, AsleacType target) {
        AsleacType[] types = target.getEffectiveAttackers();
        for (AsleacType type : types) {
            if (type == attacker)
                return true;
        }
        return false;
    }

    public abstract AsleacType[] getEffectiveAttackers();
}