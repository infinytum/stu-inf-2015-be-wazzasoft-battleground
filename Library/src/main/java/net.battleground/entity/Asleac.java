package net.battleground.entity;

import net.battleground.ability.Ability;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bteusm on 11.03.2016.
 */
public class Asleac implements Serializable {

    private int id;
    private String name;
    private String customName = "";
    private AsleacType type;
    private int health;
    private int maxHealth;
    private double level;
    private boolean alive;
    private int price = 50;
    private ArrayList<Ability> abilities = new ArrayList<Ability>();


    public boolean isAlive() {
        return health > 0;
    }

    public int getPrice() {
        return (int) Math.round(price * (1 + level / 5f));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AsleacType getType() {
        return type;
    }

    public void setType(AsleacType type) {
        this.type = type;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    public ArrayList<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(ArrayList<Ability> abilities) {
        this.abilities = abilities;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public void damage(Ability ability, Asleac attacker) {

        // ((((2 × Level) ÷ 5 + 2) * BasePower) ÷ 50 + 2
        double dam = ((((2 * attacker.getLevel()) / 5 + 2) * ability.getStrength()) / 2 + 2);
        System.out.println(dam);
        double percentOfMaxDamage = ((attacker.getLevel() / 100.0) * attacker.getLevel()) + 1.0;
        double typeBooster = 1.0;
        if (AsleacType.isEffectiveAgainst(ability.getType(), getType())) {
            typeBooster = 2.0;
        } else if (AsleacType.isEffectiveAgainst(getType(), ability.getType())) {
            typeBooster = 0.4;
        }
        double actualDamage = dam * typeBooster;

        setHealth(getHealth() - (int) actualDamage);
    }

}