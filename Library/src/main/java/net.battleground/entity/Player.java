package net.battleground.entity;

import net.battleground.Position;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bteusm on 11.03.2016.
 */
public class Player implements Serializable {

    private int id;
    private String username;
    private String email;
    private ArrayList<Position> items = new ArrayList<Position>();
    private ArrayList<Asleac> asleacs = new ArrayList<Asleac>();

    public Player() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Position> getItems() {
        return items;
    }

    public void setItems(ArrayList<Position> items) {
        this.items = items;
    }

    public ArrayList<Asleac> getAsleacs() {
        return asleacs;
    }

    public void setAsleacs(ArrayList<Asleac> asleacs) {
        this.asleacs = asleacs;
    }
}
