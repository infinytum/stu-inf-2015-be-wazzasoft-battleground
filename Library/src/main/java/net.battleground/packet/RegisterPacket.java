package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 23.03.2016.
 */
public class RegisterPacket extends Packet implements Serializable {

    private String username;
    private String password;
    private String password_rep;
    private String email;

    public RegisterPacket() {
        setId(Packets.REGISTER_PACKET.getID());
    }

    public RegisterPacket(String username, String password, String password_rep, String email) {
        setId(Packets.REGISTER_PACKET.getID());
        this.username = username;
        this.password = password;
        this.password_rep = password_rep;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPassword_rep() {
        return password_rep;
    }

    public String getEmail() {
        return email;
    }
}
