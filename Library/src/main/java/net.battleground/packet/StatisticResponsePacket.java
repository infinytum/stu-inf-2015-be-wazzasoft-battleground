package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 23.03.2016.
 */
public class StatisticResponsePacket extends Packet implements Serializable {

    private int wins;
    private int games;
    private int money;
    private int asleacs_possessed;

    public StatisticResponsePacket() {
        setId(Packets.STATISTIC_RESPONSE_PACKET.getID());
    }

    public StatisticResponsePacket(int wins, int games, int money, int asleacs_possessed) {
        setId(Packets.STATISTIC_RESPONSE_PACKET.getID());
        this.wins = wins;
        this.games = games;
        this.money = money;
        this.asleacs_possessed = asleacs_possessed;
    }

    public int getWins() {
        return wins;
    }

    public int getGames() {
        return games;
    }

    public int getMoney() {
        return money;
    }

    public int getAsleacs_possessed() {
        return asleacs_possessed;
    }
}
