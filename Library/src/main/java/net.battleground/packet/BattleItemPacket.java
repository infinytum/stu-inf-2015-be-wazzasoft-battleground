package net.battleground.packet;

import net.battleground.item.Item;
import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 30.03.2016.
 */
public class BattleItemPacket extends Packet implements Serializable {

    private Item item;

    public BattleItemPacket() {
        setId(Packets.BATTLE_ITEM_PACKET.getID());
    }

    public BattleItemPacket(Item item) {
        setId(Packets.BATTLE_ITEM_PACKET.getID());
        this.item = item;
    }

    public Item getItem() {
        return item;
    }
}
