package net.battleground.packet;

import net.battleground.entity.Player;
import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 18.03.2016.
 */
public class LoginStatusPacket extends Packet implements Serializable {

    private boolean success;
    private Player player;

    public LoginStatusPacket() {
        setId(Packets.LOGIN_STATUS_PACKET.getID());
    }

    public LoginStatusPacket(boolean success) {
        setId(Packets.LOGIN_STATUS_PACKET.getID());
        this.success = success;
    }

    public LoginStatusPacket(boolean success, Player player) {
        setId(Packets.LOGIN_STATUS_PACKET.getID());
        this.success = success;
        this.player = player;
    }

    public boolean isSuccess() {
        return success;
    }

    public Player getPlayer() {
        return player;
    }
}
