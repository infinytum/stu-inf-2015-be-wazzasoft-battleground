package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by Michael on 28.03.2016.
 */
public class BuyItemRequestPacket extends Packet implements Serializable {

    private int item_id;

    public BuyItemRequestPacket() {
        setId(Packets.BUY_ITEM_REQUEST_PACKET.getID());
    }

    public BuyItemRequestPacket(int item_id) {
        setId(Packets.BUY_ITEM_REQUEST_PACKET.getID());
        this.item_id = item_id;
    }

    public int getItem_id() {
        return item_id;
    }
}
