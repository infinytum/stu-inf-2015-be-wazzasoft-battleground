package net.battleground.packet;

import net.battleground.Position;
import net.battleground.entity.Asleac;
import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Michael on 27.03.2016.
 */
public class ShopResponsePacket extends Packet implements Serializable {
    private int money;
    private ArrayList<Asleac> ownedAsleacs;
    private ArrayList<Position> items;
    private ArrayList<Asleac> buyableAsleacs;

    public ShopResponsePacket() {
        setId(Packets.SHOP_RESPONSE_PACKET.getID());
    }

    public ShopResponsePacket(int money, ArrayList<Asleac> ownedAsleacs, ArrayList<Asleac> buyableAsleacs, ArrayList<Position> items) {
        setId(Packets.SHOP_RESPONSE_PACKET.getID());
        this.money = money;
        this.buyableAsleacs = buyableAsleacs;
        this.ownedAsleacs = ownedAsleacs;
        this.items = items;
    }

    public int getMoney() {
        return money;
    }

    public ArrayList<Asleac> getOwnedAsleacs() {
        return ownedAsleacs;
    }

    public ArrayList<Position> getItems() {
        return items;
    }

    public ArrayList<Asleac> getBuyableAsleacs() {
        return buyableAsleacs;
    }


}
