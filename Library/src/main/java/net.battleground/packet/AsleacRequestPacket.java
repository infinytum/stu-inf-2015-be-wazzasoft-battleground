package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by mkteu on 28.03.2016.
 */
public class AsleacRequestPacket extends Packet implements Serializable {

    public AsleacRequestPacket() {
        setId(Packets.ASELAC_REQUEST_PACKET.getID());
    }

}
