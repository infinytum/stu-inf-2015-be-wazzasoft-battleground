package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 23.03.2016.
 */
public class StatisticRequestPacket extends Packet implements Serializable {

    public StatisticRequestPacket() {
        setId(Packets.STATISTIC_REQUEST_PACKET.getID());
    }

}
