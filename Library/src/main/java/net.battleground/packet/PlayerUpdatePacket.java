package net.battleground.packet;

import net.battleground.entity.Player;
import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 30.03.2016.
 */
public class PlayerUpdatePacket extends Packet implements Serializable {

    private Player player;

    public PlayerUpdatePacket() {
        setId(Packets.PLAYER_UPDATE_PACKET.getID());
    }

    public PlayerUpdatePacket(Player player) {
        setId(Packets.PLAYER_UPDATE_PACKET.getID());
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
