package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 18.03.2016.
 */
public class LogoutPacket extends Packet implements Serializable {

    public LogoutPacket() {
        setId(Packets.LOGOUT_PACKET.getID());
    }

}
