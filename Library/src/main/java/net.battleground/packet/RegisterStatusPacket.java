package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 23.03.2016.
 */
public class RegisterStatusPacket extends Packet implements Serializable {

    private boolean success;
    private String reason = "";

    public RegisterStatusPacket() {
        setId(Packets.REGISTER_STATUS_PACKET.getID());
    }

    public RegisterStatusPacket(boolean success, String reason) {
        setId(Packets.REGISTER_STATUS_PACKET.getID());
        this.success = success;
        this.reason = reason;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getReason() {
        return reason;
    }
}
