package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by Michael on 27.03.2016.
 */
public class ShopRequestPacket extends Packet implements Serializable {

    public ShopRequestPacket() {
        setId(Packets.SHOP_REQUEST_PACKET.getID());
    }
}
