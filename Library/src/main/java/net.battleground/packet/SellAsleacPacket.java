package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bwaltm on 29.03.2016.
 */
public class SellAsleacPacket extends Packet implements Serializable {

    private int asleacID;

    public SellAsleacPacket() {
        this.setId(Packets.SELL_ASLEAC_PACKET.getID());
    }

    public SellAsleacPacket(int asleacID) {
        this.setId(Packets.SELL_ASLEAC_PACKET.getID());
        this.asleacID = asleacID;
    }

    public int getAsleacID() {
        return asleacID;
    }
}
