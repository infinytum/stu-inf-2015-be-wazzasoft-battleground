package net.battleground.packet;

import net.battleground.entity.Asleac;
import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 16.03.2016.
 */
public class BattleInfoPacket extends Packet implements Serializable {

    private BattleState battleState;
    private String winner;
    private String loser;

    private Asleac enemyAsleac;
    private Asleac ownAsleac;
    private String enemyName;

    public BattleInfoPacket() {
        setId(Packets.BATTLE_INFO_PACKET.getID());
    }

    public BattleInfoPacket(BattleState battleState) {
        setId(Packets.BATTLE_INFO_PACKET.getID());
        this.battleState = battleState;
    }

    public BattleInfoPacket(BattleState battleState, String winner, String loser) {
        setId(Packets.BATTLE_INFO_PACKET.getID());
        this.battleState = battleState;
        this.winner = winner;
        this.loser = loser;
    }

    public BattleInfoPacket(BattleState state, Asleac enemyAsleac, Asleac ownAsleac, String enemyName) {
        setId(Packets.BATTLE_INFO_PACKET.getID());
        this.battleState = state;
        this.enemyAsleac = enemyAsleac;
        this.ownAsleac = ownAsleac;
        this.enemyName = enemyName;
    }

    public BattleState getBattleState() {
        return battleState;
    }

    public String getWinner() {
        return winner;
    }

    public String getLoser() {
        return loser;
    }

    public Asleac getEnemyAsleac() {
        return enemyAsleac;
    }

    public Asleac getOwnAsleac() {
        return ownAsleac;
    }

    public String getEnemyName() {
        return enemyName;
    }

    public enum BattleState {
        WAITING, BEGIN, RUNNNING, END, QUIT_WAIT, SURRENDER, DIE
    }

}
