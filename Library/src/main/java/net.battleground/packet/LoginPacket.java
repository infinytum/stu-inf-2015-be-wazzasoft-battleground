package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 16.03.2016.
 */
public class LoginPacket extends Packet implements Serializable {

    private String username;
    private String password;


    public LoginPacket() {
        setId(Packets.LOGIN_PACKET.getID());
    }

    public LoginPacket(String username, String password) {
        setId(Packets.LOGIN_PACKET.getID());
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
