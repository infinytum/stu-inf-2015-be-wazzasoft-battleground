package net.battleground.packet;

import net.battleground.entity.Asleac;
import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bwaltm on 29.03.2016.
 */
public class BuyAsleacPacket extends Packet implements Serializable {

    private Asleac asleac;
    private String customName;

    public BuyAsleacPacket() {
        setId(Packets.BUY_ASLEAC_PACKET.getID());
    }

    public BuyAsleacPacket(Asleac asleac, String customName) {
        setId(Packets.BUY_ASLEAC_PACKET.getID());
        this.asleac = asleac;
        this.customName = customName;
    }

    public BuyAsleacPacket(String customName) {
        setId(Packets.BUY_ASLEAC_PACKET.getID());
        this.customName = customName;
    }

    public Asleac getAsleac() {
        return asleac;
    }

    public String getCustomName() {
        return customName;
    }


}
