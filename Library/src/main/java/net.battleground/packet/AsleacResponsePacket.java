package net.battleground.packet;

import net.battleground.entity.Asleac;
import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mkteu on 28.03.2016.
 */
public class AsleacResponsePacket extends Packet implements Serializable {

    private ArrayList<Asleac> asleacs;

    public AsleacResponsePacket() {
        setId(Packets.ASLEAC_RESPONSE_PACKET.getID());
    }

    public AsleacResponsePacket(ArrayList<Asleac> asleacs) {
        setId(Packets.ASLEAC_RESPONSE_PACKET.getID());
        this.asleacs = asleacs;
    }

    public ArrayList<Asleac> getAsleacs() {
        return asleacs;
    }
}
