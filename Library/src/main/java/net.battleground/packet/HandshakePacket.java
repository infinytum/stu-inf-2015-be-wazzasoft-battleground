package net.battleground.packet;

import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by bteusm on 18.03.2016.
 */
public class HandshakePacket extends Packet implements Serializable {

    private int clientID;

    public HandshakePacket() {
        setId(Packets.HANDSHAKE.getID());
    }

    public HandshakePacket(int clientID) {
        setId(Packets.HANDSHAKE.getID());
        this.clientID = clientID;
    }

    public int getClientID() {
        return clientID;
    }
}
