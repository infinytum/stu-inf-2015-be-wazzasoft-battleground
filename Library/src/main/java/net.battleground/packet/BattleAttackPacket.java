package net.battleground.packet;

import net.battleground.ability.Ability;
import net.battleground.network.Packet;
import net.battleground.network.Packets;

import java.io.Serializable;

/**
 * Created by mkteu on 25.03.2016.
 */
public class BattleAttackPacket extends Packet implements Serializable {

    private Ability usedAbility;

    public BattleAttackPacket() {
        setId(Packets.BATTLE_ATTACK_PACKET.getID());
    }

    public BattleAttackPacket(Ability usedAbility) {
        setId(Packets.BATTLE_ATTACK_PACKET.getID());
        this.usedAbility = usedAbility;
    }

    public Ability getUsedAbility() {
        return usedAbility;
    }
}
