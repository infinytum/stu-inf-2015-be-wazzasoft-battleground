package net.battleground;

/**
 * Created by Michael on 17.03.2016.
 */
public enum LogLevel {

    INFO, WARNING, ERROR, DEBUG

}
