package net.battleground.callback;

/**
 * Created by mkteu on 25.03.2016.
 */
public interface Callback<T> {

    public void onCallback(T packet);

}
