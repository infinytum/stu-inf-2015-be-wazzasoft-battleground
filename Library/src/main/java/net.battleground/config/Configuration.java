package net.battleground.config;

import net.battleground.Logger;

import java.io.*;
import java.util.Properties;

/**
 * Created by bteusm on 31.03.2016.
 */
public abstract class Configuration {

    private Properties prop;
    private String filename;

    protected Configuration(String filename) {
        this.filename = filename;
    }

    public void load() {
        InputStream inputStream = null;
        try {
            prop = new Properties();

            inputStream = new FileInputStream(new File(filename));
            prop.load(inputStream);
        } catch (Exception e) {
            Logger.error("Can't find configuration file: " + filename + " !");
            System.out.println("Exception: " + e);
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public String get(String key) {
        if (prop == null)
            load();
        return prop.getProperty(key);
    }

    public void set(String key, String value) {
        if (prop == null)
            load();
        prop.setProperty(key, value);
    }

    public void save() {
        try {
            prop.store(new FileOutputStream(new File(filename)), "Battleground Configuration File");
        } catch (IOException e) {
        }
    }

}
