package net.battleground.network;

import net.battleground.packet.*;

import java.io.Serializable;

public enum Packets implements Serializable {

    LOGIN_PACKET {
        public int getID() {
            return 3;
        }

        public Class<? extends Packet> getPacketClass() {
            return LoginPacket.class;
        }
    },
    LOGIN_STATUS_PACKET {
        public int getID() {
            return 4;
        }

        public Class<? extends Packet> getPacketClass() {
            return LoginStatusPacket.class;
        }
    },
    HANDSHAKE {
        public int getID() {
            return 2;
        }

        public Class<? extends Packet> getPacketClass() {
            return HandshakePacket.class;
        }
    },
    LOGOUT_PACKET {
        public int getID() {
            return 5;
        }

        public Class<? extends Packet> getPacketClass() {
            return LogoutPacket.class;
        }
    },
    REGISTER_PACKET {
        public int getID() {
            return 6;
        }

        public Class<? extends Packet> getPacketClass() {
            return RegisterPacket.class;
        }
    },
    REGISTER_STATUS_PACKET {
        public int getID() {
            return 7;
        }

        public Class<? extends Packet> getPacketClass() {
            return RegisterStatusPacket.class;
        }
    },
    STATISTIC_REQUEST_PACKET {
        public int getID() {
            return 8;
        }

        public Class<? extends Packet> getPacketClass() {
            return StatisticRequestPacket.class;
        }
    },
    STATISTIC_RESPONSE_PACKET {
        public int getID() {
            return 9;
        }

        public Class<? extends Packet> getPacketClass() {
            return StatisticResponsePacket.class;
        }
    },
    BATTLE_INFO_PACKET {
        public int getID() {
            return 10;
        }

        public Class<? extends Packet> getPacketClass() {
            return BattleInfoPacket.class;
        }
    },
    BATTLE_ATTACK_PACKET {
        public int getID() {
            return 11;
        }

        public Class<? extends Packet> getPacketClass() {
            return BattleAttackPacket.class;
        }
    },
    SHOP_REQUEST_PACKET {
        public int getID() {
            return 12;
        }

        public Class<? extends Packet> getPacketClass() {
            return ShopRequestPacket.class;
        }
    },
    SHOP_RESPONSE_PACKET {
        public int getID() {
            return 13;
        }

        public Class<? extends Packet> getPacketClass() {
            return ShopResponsePacket.class;
        }
    },
    BUY_ITEM_REQUEST_PACKET {
        public int getID() {
            return 14;
        }

        public Class<? extends Packet> getPacketClass() {
            return BuyItemRequestPacket.class;
        }
    },
    ASELAC_REQUEST_PACKET {
        public int getID() {
            return 15;
        }

        public Class<? extends Packet> getPacketClass() {
            return AsleacRequestPacket.class;
        }
    },
    ASLEAC_RESPONSE_PACKET {
        public int getID() {
            return 16;
        }

        public Class<? extends Packet> getPacketClass() {
            return AsleacResponsePacket.class;
        }
    },
    BUY_ASLEAC_PACKET {
        public int getID() {
            return 17;
        }

        public Class<? extends Packet> getPacketClass() {
            return BuyAsleacPacket.class;
        }
    },
    SELL_ASLEAC_PACKET {
        public int getID() {
            return 18;
        }

        public Class<? extends Packet> getPacketClass() {
            return SellAsleacPacket.class;
        }
    },
    PLAYER_UPDATE_PACKET {
        public int getID() {
            return 19;
        }

        public Class<? extends Packet> getPacketClass() {
            return PlayerUpdatePacket.class;
        }
    },
    BATTLE_ITEM_PACKET {
        public int getID() {
            return 20;
        }

        public Class<? extends Packet> getPacketClass() {
            return BattleItemPacket.class;
        }
    };


    public static Packets getById(int id) {
        for (Packets p : values()) {
            if (p.getID() == id)
                return p;
        }
        return null;
    }

    public abstract int getID();

    public abstract Class<? extends Packet> getPacketClass();

}
