package net.battleground.network;

import java.io.Serializable;

/**
 * Created by bteusm on 16.03.2016.
 */
public class Packet implements Serializable {

    private int id;
    private int sender = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSender() {
        return sender;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }
}

