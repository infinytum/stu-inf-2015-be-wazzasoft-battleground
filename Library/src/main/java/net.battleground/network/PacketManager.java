package net.battleground.network;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import net.battleground.listener.Listener;
import net.battleground.listener.PacketHandler;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bteusm on 16.03.2016.
 */
public class PacketManager {


    private static HashMap<Integer, ArrayList<Method>> registeredListener = new HashMap<Integer, ArrayList<Method>>();
    private static HashMap<Method, Listener> listenerMap = new HashMap<Method, Listener>();


    public static void handlePacket(Packet packet) {
        HashMap<Integer, ArrayList<Method>> registeredListener = new HashMap<>(PacketManager.registeredListener);  // Cloning hashmaps so we can modify the original ones while looping
        HashMap<Method, Listener> listenerMap = new HashMap<>(PacketManager.listenerMap);
        int id = packet.getId();
        try {
            if (registeredListener.containsKey(id)) {   // Check for waiting listeners
                Method[] listeners = registeredListener.get(id).toArray(new Method[0]); // Get all registered methods
                for (Method m : listeners) {    // Loop through
                    m.setAccessible(true);
                    m.invoke(listenerMap.get(m), packet);   // Invoke and pass the packet
                }
            }

        } catch (IllegalAccessException | InvocationTargetException ex) {
            ex.printStackTrace();
        }
    }


    public static void registerListener(Listener listener) {
        for (Method m : listener.getClass().getDeclaredMethods()) { // Get all declared methods
            if (m.isAnnotationPresent(PacketHandler.class)) {   // Check for @PacketHandler Method
                Class<? extends Packet> clazz = m.getParameterTypes()[0].asSubclass(Packet.class);  // Get wanted Packet
                try {
                    Packet p = clazz.newInstance();
                    registerMethod(m, p.getId());   // Register method for the wanted packet
                    listenerMap.put(m, listener);
                } catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                } catch (InstantiationException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void unregisterListener(Listener listener) {      // Same as register but with removing instead of adding to lists
        for (Method m : listener.getClass().getDeclaredMethods()) {
            if (m.isAnnotationPresent(PacketHandler.class)) {
                Class<? extends Packet> clazz = m.getParameterTypes()[0].asSubclass(Packet.class);
                try {
                    Packet p = clazz.newInstance();
                    unregisterMethod(m, p.getId());
                    listenerMap.remove(m);
                } catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                } catch (InstantiationException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private static void registerMethod(Method m, int id) {
        ArrayList<Method> listener = new ArrayList<Method>();
        if (registeredListener.containsKey(id))
            listener = registeredListener.get(id);

        listener.add(m);
        registeredListener.remove(id);
        registeredListener.put(id, listener);
    }

    private static void unregisterMethod(Method m, int id) {
        ArrayList<Method> listener = new ArrayList<Method>();
        if (registeredListener.containsKey(id))
            listener = registeredListener.get(id);

        listener.remove(m);
        registeredListener.remove(id);
        registeredListener.put(id, listener);
    }

    public static void sendPacket(Packet packet, Channel channel) {
        ByteBuf buf = Unpooled.buffer();
        buf.writeBytes(convertToBytes(packet));
        channel.writeAndFlush(packet);
    }

    private static byte[] convertToBytes(Object object) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Object convertFromBytes(byte[] bytes) throws ClassNotFoundException, IOException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }

}
