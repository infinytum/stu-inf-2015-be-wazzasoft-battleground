package net.battleground.test;

import net.battleground.listener.Listener;
import net.battleground.listener.PacketHandler;
import net.battleground.network.PacketManager;
import net.battleground.packet.LogoutPacket;
import org.junit.Assert;

/**
 * Created by mkteu on 27.03.2016.
 */
public class PacketManagerTest {

    public boolean listenerCalled;

    @org.junit.Test
    public void handlePacket() throws Exception {
        Listener listener = new Listener() {

            @PacketHandler
            public void onPacket(LogoutPacket packet) {
                listenerCalled = true;
            }

        };
        PacketManager.registerListener(listener);
        PacketManager.handlePacket(new LogoutPacket());
        PacketManager.unregisterListener(listener);
        Assert.assertTrue(listenerCalled);
        listenerCalled = false;
    }

    @org.junit.Test
    public void registerListener() throws Exception {
        Listener listener = new Listener() {

            @PacketHandler
            public void onPacket(LogoutPacket packet) {
                listenerCalled = true;
            }

        };
        PacketManager.registerListener(listener);
        PacketManager.handlePacket(new LogoutPacket());
        PacketManager.unregisterListener(listener);
        Assert.assertTrue(listenerCalled);
        listenerCalled = false;
    }

    @org.junit.Test
    public void unregisterListener() throws Exception {
        listenerCalled = false;
        Listener listener = new Listener() {

            @PacketHandler
            public void onPacket(LogoutPacket packet) {
                listenerCalled = true;
            }

        };
        PacketManager.registerListener(listener);
        PacketManager.unregisterListener(listener);
        PacketManager.handlePacket(new LogoutPacket());
        Assert.assertFalse(listenerCalled);
    }

}