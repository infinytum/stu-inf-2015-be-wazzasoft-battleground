package net.battleground.test;

import net.battleground.config.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by bteusm on 31.03.2016.
 */
public class ConfigurationTest {

    Configuration cfg;

    @Before
    public void initConfigFile() {
        cfg = new Configuration("test.cfg") {
        };
        cfg.set("Test", "HalloWelt");
        cfg.save();

        cfg = new Configuration("test.cfg") {
        };
    }

    @Test
    public void load() throws Exception {
        cfg.load();
        Assert.assertEquals("HalloWelt", cfg.get("Test"));
    }

    @Test
    public void get() throws Exception {
        Assert.assertEquals("HalloWelt", cfg.get("Test"));
    }

    @Test
    public void set() throws Exception {
        cfg.set("Test", "HalloMond");
        cfg.save();
        Assert.assertEquals("HalloMond", cfg.get("Test"));
    }

    @Test
    public void save() throws Exception {
        cfg.set("Test", "HalloSonne");
        cfg.save();
        Assert.assertEquals("HalloSonne", cfg.get("Test"));
    }
}