package net.battleground.test;

import net.battleground.entity.AsleacType;
import org.junit.Assert;
import org.junit.Test;

public class AsleacTypeTest {

    @Test
    public void getTypeFromString() throws Exception {
        Assert.assertTrue(AsleacType.ELECTRIC == AsleacType.valueOf("ELECTRIC"));
    }

    @Test
    public void getTypeFromStringEx() throws Exception {
        Assert.assertTrue(AsleacType.ELECTRIC == AsleacType.valueOf("FAIL"));
    }

    @Test
    public void isEffectiveAgainst() throws Exception {
        Assert.assertTrue(AsleacType.isEffectiveAgainst(AsleacType.WATER, AsleacType.FIRE));
    }
}