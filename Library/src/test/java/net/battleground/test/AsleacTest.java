package net.battleground.test;

import net.battleground.entity.Asleac;
import net.battleground.entity.AsleacType;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mkteu on 28.03.2016.
 */
public class AsleacTest {

    public Asleac a = new Asleac();

    @Test
    public void isAlive() throws Exception {
        a.setHealth(0);
        Assert.assertFalse(a.isAlive());
    }

    @Test
    public void getPrice() throws Exception {
    }

    @Test
    public void getId() throws Exception {
        a.setId(3);
        Assert.assertEquals(3, a.getId());
    }

    @Test
    public void setId() throws Exception {
        a.setId(4);
        Assert.assertNotEquals(3, a.getId());
    }

    @Test
    public void getName() throws Exception {
        a.setName("Max");
        Assert.assertEquals(a.getName(), "Max");
    }

    @Test
    public void setName() throws Exception {
        a.setName("GibbiX");
        Assert.assertNotEquals(a.getName(), "Max");
    }

    @Test
    public void getType() throws Exception {
        a.setType(AsleacType.FIGHT);
        Assert.assertEquals(AsleacType.FIGHT, a.getType());
    }

    @Test
    public void setType() throws Exception {
        a.setType(AsleacType.FIRE);
        Assert.assertNotEquals(AsleacType.FIGHT, a.getType());
    }

    @Test
    public void getHealth() throws Exception {
        a.setHealth(20);
        Assert.assertEquals(20, a.getHealth());
    }

    @Test
    public void setHealth() throws Exception {
        a.setHealth(30);
        Assert.assertNotEquals(20, a.getHealth());
    }

    @Test
    public void getMaxHealth() throws Exception {
        a.setMaxHealth(200);
        Assert.assertEquals(200, a.getMaxHealth());
    }

    @Test
    public void setMaxHealth() throws Exception {
        a.setMaxHealth(300);
        Assert.assertNotEquals(200, a.getMaxHealth());
    }

    @Test
    public void getLevel() throws Exception {
        a.setLevel(30);
        Assert.assertEquals(30.0, a.getLevel(), 0);
    }

    @Test
    public void setLevel() throws Exception {
        a.setLevel(40);
        Assert.assertNotEquals(30, a.getLevel());
    }

    @Test
    public void getAbilities() throws Exception {

    }

    @Test
    public void setAbilities() throws Exception {

    }

    @Test
    public void getCustomName() throws Exception {
        a.setCustomName("Maxi");
        Assert.assertEquals("Maxi", a.getCustomName());
    }

    @Test
    public void setCustomName() throws Exception {
        a.setCustomName("GibbiX");
        Assert.assertNotEquals("Maxi", a.getCustomName());
    }

    @Test
    public void damage() throws Exception {

    }
}