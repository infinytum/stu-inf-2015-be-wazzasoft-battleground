package net.battleground.library;

/**
 * Created by mkteu on 21.03.2016.
 */
public class Font extends java.awt.Font {

    public Font(String name, int style, int size) {
        super(name, style, ScreenScaler.getScaledInt(size));
    }

}
