package net.battleground.library;

/**
 * Created by mkteu on 21.03.2016.
 */
public class Dimension extends java.awt.Dimension {

    public Dimension(int width, int height) {
        setSize(ScreenScaler.getScaledInt(width), ScreenScaler.getScaledInt(height));
    }

}
