package net.battleground.library;


/**
 * Created by mkteu on 21.03.2016.
 */
public class ScreenScaler {

    public static double DPI = java.awt.Toolkit.getDefaultToolkit().getScreenResolution() / 100.0;
    private static java.awt.Dimension fullscreen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
    private static double originalWidth = 1920.0;
    private static double originalHeight = 1080.0;


    private ScreenScaler() {
    }

    public static int getScaledInt(int size) {
        return (int) (size * DPI);
    }

    public static double getScaledDouble(double size) {
        return (size * DPI);
    }

    public static java.awt.Dimension getScale(java.awt.Dimension dim) {
        java.awt.Dimension newDim = new java.awt.Dimension();
        double newWidth = ((((100.0 / originalWidth) * fullscreen.getWidth()) / 100.0) * dim.getWidth()); // Calculate new Dimension based on how much bigger / smaller the screen is related to the development screen
        double newHeight = (((100.0 / originalHeight) * fullscreen.getHeight()) / 100.0) * dim.getHeight();
        newDim.setSize(newWidth, newHeight);
        return newDim;
    }
/*
    public static int getScaledHeight(int originalHeigth){

    }
*/
}
