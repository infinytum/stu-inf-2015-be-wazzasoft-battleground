package net.battleground.gui.component;

import net.battleground.Position;
import net.battleground.factory.AssetFactory;
import net.battleground.item.Item;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 11.03.2016.
 */
public class ShopItem extends JLabel {

    private Image background = AssetFactory.loadImageAsset("ShopItem.png");
    private JLabel owned;
    private int amount;

    public ShopItem(Position position) {
        Item item = position.getItem();
        amount = position.getAmount();
        String name = item.getName();
        setLayout(new OverlayLayout(this));
        Image image = background.getScaledInstance(168, 110, Image.SCALE_SMOOTH);
        setIcon(new ImageIcon(image));

        JLabel itemImageLabel = new JLabel();
        Image itemImage = AssetFactory.loadItemAsset(name + ".png");
        itemImage = itemImage.getScaledInstance(64, 64, Image.SCALE_SMOOTH);
        itemImageLabel.setIcon(new ImageIcon(itemImage));
        itemImageLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        add(itemImageLabel);

        JLabel price = new JLabel(item.getPrice() + "$");
        price.setBorder(BorderFactory.createEmptyBorder(0, 125, 6, 0));
        JLabel nameLabel = new JLabel(name);
        nameLabel.setBorder(BorderFactory.createEmptyBorder(0, 40, 70, 0));
        owned = new JLabel("owned: " + amount);
        owned.setBorder(BorderFactory.createEmptyBorder(67, 30, 0, 0));

        add(price);
        add(nameLabel);
        add(owned);
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
        owned.setText("owned: " + amount);
    }


}
