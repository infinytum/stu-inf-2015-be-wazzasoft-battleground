package net.battleground.gui.component;

import net.battleground.entity.Asleac;
import net.battleground.factory.AssetFactory;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bwaltm on 29.03.2016.
 */
public class ShopAsleac extends JLabel {

    private Image background = AssetFactory.loadImageAsset("ShopItem.png");

    public ShopAsleac(Asleac asleac) {
        String name = asleac.getName();
        String customName = asleac.getCustomName();
        boolean owned = true;
        if (customName.isEmpty() || customName == null) {
            owned = false;
        }

        setLayout(new OverlayLayout(this));
        Image image = background.getScaledInstance(168, 110, Image.SCALE_SMOOTH);

        setIcon(new ImageIcon(image));

        JLabel itemImageLabel = new JLabel();
        Image itemImage = AssetFactory.loadAsleacAsset("Front" + name + ".png");
        itemImage = itemImage.getScaledInstance(64, 64, Image.SCALE_SMOOTH);
        itemImageLabel.setIcon(new ImageIcon(itemImage));
        itemImageLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        add(itemImageLabel);

        JLabel price = new JLabel();
        if (owned) {
            price.setText("Sell For:   " + asleac.getPrice() + "$");
            price.setBorder(BorderFactory.createEmptyBorder(0, 70, 6, 0));
        } else {
            price.setText(asleac.getPrice() + "$");
            price.setBorder(BorderFactory.createEmptyBorder(0, 125, 6, 0));
        }
        add(price);

        JLabel nameLabel = new JLabel();
        nameLabel.setBorder(BorderFactory.createEmptyBorder(0, 40, 70, 0));
        nameLabel.setText(owned ? customName : name);
        add(nameLabel);


    }
}
