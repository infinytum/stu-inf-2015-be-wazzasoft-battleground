package net.battleground.gui.component;

import net.battleground.client.BattleClient;
import net.battleground.entity.Asleac;
import net.battleground.gui.screen.ShopScreen;
import net.battleground.packet.SellAsleacPacket;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bwaltm on 29.03.2016.
 */
public class SellAsleac extends JDialog {

    public SellAsleac(ShopScreen shopscreen, Asleac asleac, ShopAsleac shopAsleac) {
        shopscreen.setEnabled(false);
        setLocationRelativeTo(null);
        JLabel message = new JLabel("Are you sure you want to sell your Asleac?");
        JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton sell = new JButton("Sell");
        JButton cancel = new JButton("Cancel");
        buttons.add(sell);
        buttons.add(cancel);

        cancel.addActionListener(e -> {
            dispose();
            shopscreen.setEnabled(true);
            shopscreen.toFront();
        });
        sell.addActionListener(e -> {
            BattleClient.sendPacket(new SellAsleacPacket(asleac.getId()));
            shopscreen.setMoney(shopscreen.getMoney() + asleac.getPrice());
            shopscreen.asleacCount = shopscreen.asleacCount - 1;
            Container container = shopAsleac.getParent();
            container.remove(shopAsleac);
            dispose();
            shopscreen.setEnabled(true);
            shopscreen.toFront();
            shopscreen.repaint();
        });
        add(message, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);
        pack();
        setVisible(true);
    }
}
