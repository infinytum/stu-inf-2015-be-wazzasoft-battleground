package net.battleground.gui.component;

import net.battleground.entity.Asleac;
import net.battleground.factory.AssetFactory;
import net.battleground.library.Dimension;
import net.battleground.library.Font;
import net.battleground.library.ScreenScaler;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 17.03.2016.
 */
public class StatusBar extends JLabel {

    private JLabel nameLabel = new JLabel();
    private JLabel levelLabel = new JLabel();
    private HealthBar healthBar;

    private Asleac asleac;
    private Image icon = AssetFactory.loadImageAsset("StatusDisplay.png").getScaledInstance(ScreenScaler.getScaledInt(282), ScreenScaler.getScaledInt(44), Image.SCALE_SMOOTH);


    public StatusBar(Asleac asleac) {
        this.asleac = asleac;
        healthBar = new HealthBar(asleac);
        setMaximumSize(new Dimension(282, 44));
        setMinimumSize(new Dimension(282, 44));
        setPreferredSize(new Dimension(282, 44));
        setSize(new Dimension(282, 44));

        nameLabel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(-3), ScreenScaler.getScaledInt(10), ScreenScaler.getScaledInt(10), ScreenScaler.getScaledInt(5)));
        nameLabel.setFont(new Font("Arial", Font.BOLD, 14));
        nameLabel.setText(asleac.getCustomName());

        levelLabel.setFont(new Font("Arial", Font.BOLD, 13));
        levelLabel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(27), ScreenScaler.getScaledInt(80), 0, 0));
        levelLabel.setText("Lvl " + (int) asleac.getLevel());

        healthBar.setBorder(BorderFactory.createEmptyBorder(0, ScreenScaler.getScaledInt(174), ScreenScaler.getScaledInt(6), 0));


        add(nameLabel);
        add(levelLabel);
        add(healthBar);

        setLayout(new OverlayLayout(this));
        setIcon(new ImageIcon(icon));
    }

}
