package net.battleground.gui.component;

import net.battleground.client.BattleClient;
import net.battleground.entity.Asleac;
import net.battleground.gui.screen.Screen;
import net.battleground.gui.screen.ShopScreen;
import net.battleground.packet.BuyAsleacPacket;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bwaltm on 29.03.2016.
 */
public class BuyAsleac extends JDialog {
    private Asleac asleac;
    private ShopScreen shopScreen;
    private Screen screen;

    //constructor for ShopScreen
    public BuyAsleac(ShopScreen shopScreen, Asleac asleac) {
        this.shopScreen = shopScreen;
        this.asleac = asleac;
        init();
    }

    //constructor for first Asleac
    public BuyAsleac(Screen screen) {
        this.screen = screen;
        init();
    }

    private void init() {
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        JPanel page = new JPanel(new BorderLayout());
        page.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        setLocationRelativeTo(null);
        JLabel message = new JLabel("Enter the name of your new Asleac");
        page.add(message, BorderLayout.NORTH);

        JPanel customNamePanel = new JPanel();
        JTextField customName = new JTextField(16);
        customNamePanel.add(customName);
        page.add(customNamePanel, BorderLayout.CENTER);

        JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton create = new JButton("Buy");
        create.addActionListener(e -> {
            if (customName.getText().length() > 0 && customName.getText().length() <= 16) {
                if (shopScreen != null) {
                    if (shopScreen.getMoney() >= asleac.getPrice()) {
                        BattleClient.sendPacket(new BuyAsleacPacket(asleac, customName.getText()));
                        shopScreen.setMoney(shopScreen.getMoney() - asleac.getPrice());
                        shopScreen.asleacCount = shopScreen.asleacCount + 1;
                        asleac.setLevel(1);
                        asleac.setCustomName(customName.getText());
                        shopScreen.addOwnedAsleac(asleac);
                        dispose();
                        shopScreen.setEnabled(true);
                        shopScreen.toFront();
                    } else {
                        JOptionPane.showMessageDialog(this,
                                "You don't have enough money to buy a new Asleac",
                                "",
                                JOptionPane.WARNING_MESSAGE);
                    }
                } else {
                    BattleClient.sendPacket(new BuyAsleacPacket(customName.getText()));
                    dispose();
                    screen.setEnabled(true);
                    screen.toFront();
                }
            } else {
                JOptionPane.showMessageDialog(this,
                        "The name must be between 1 and 16 characters long",
                        "Namelength Warning",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        buttons.add(create);

        if (shopScreen != null) {
            shopScreen.setEnabled(false);
            JButton close = new JButton("Close");
            close.addActionListener(e -> {
                dispose();
                shopScreen.setEnabled(true);
                shopScreen.toFront();
            });
            buttons.add(close);
        } else {
            screen.setEnabled(false);
        }

        page.add(buttons, BorderLayout.SOUTH);
        add(page);
        setVisible(true);
        pack();
    }
}
