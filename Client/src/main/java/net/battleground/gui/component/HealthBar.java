package net.battleground.gui.component;

import net.battleground.entity.Asleac;
import net.battleground.factory.AssetFactory;
import net.battleground.library.ScreenScaler;

import javax.swing.*;
import java.awt.*;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The HealthBar object represents the red-yellow-green bar in fights and azleac overview
 * <p>
 * Its basically an JLabel with advanced functions
 */
public class HealthBar extends JLabel {

    private String texture = "HealthBar.png";
    private Asleac asleac;
    private int health = 96;

    public HealthBar(Asleac asleac) {
        setHorizontalAlignment(LEFT);
        Image image = AssetFactory.loadImageAsset(texture);
        image = image.getScaledInstance(ScreenScaler.getScaledInt(96), ScreenScaler.getScaledInt(6), Image.SCALE_SMOOTH);
        image = createImage(new FilteredImageSource(image.getSource(), new CropImageFilter(0, 0, ScreenScaler.getScaledInt(96), ScreenScaler.getScaledInt(6))));
        setIcon(new ImageIcon(image));
        this.asleac = asleac;
        mainloop();
    }

    private void mainloop() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (getAsleac().getHealth() != health) {
                    setHealth(getAsleac().getHealth());
                }
            }
        }, 0, 1000);
    }

    public Asleac getAsleac() {
        return asleac;
    }

    /**
     * Control the health which is being displayed.
     *
     * @param hp The amount Lifepoints you want to set
     */
    public void setHealth(int hp) {
        double part = (((double) ScreenScaler.getScaledInt(96)) / (double) ScreenScaler.getScaledInt(getAsleac().getMaxHealth())); // Calculate how much one HP is on the image
        Image original = AssetFactory.loadImageAsset(texture);
        Image image;

        if (hp > health) {
            for (; health < hp; ++health) {
                image = original.getScaledInstance(ScreenScaler.getScaledInt(96), ScreenScaler.getScaledInt(6), Image.SCALE_SMOOTH);    // Scale the image so it fits the space in statusbar
                image = createImage(new FilteredImageSource(image.getSource(), new CropImageFilter(0, 0, (int) Math.ceil((part * health)), ScreenScaler.getScaledInt(6)))); // Now Crop the image matching to the HP of the asleac
                setIcon(new ImageIcon(image));
                repaint();
            }
        }
        if (hp < health) {
            for (; health >= hp; --health) {
                image = original.getScaledInstance(ScreenScaler.getScaledInt(96), ScreenScaler.getScaledInt(6), Image.SCALE_SMOOTH);
                image = createImage(new FilteredImageSource(image.getSource(), new CropImageFilter(0, 0, (int) Math.ceil((part * health)), ScreenScaler.getScaledInt(6))));
                setIcon(new ImageIcon(image));
                repaint();
            }
        }
    }


}
