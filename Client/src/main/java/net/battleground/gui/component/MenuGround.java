package net.battleground.gui.component;

import net.battleground.factory.AssetFactory;
import net.battleground.library.ScreenScaler;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 17.03.2016.
 */
public class MenuGround extends Ground {

    public MenuGround() {
        super(AssetFactory.loadImageAsset("ArenaGround.png").getScaledInstance(ScreenScaler.getScaledInt(256), ScreenScaler.getScaledInt(128), Image.SCALE_SMOOTH));
        setAsleacTexture(AssetFactory.loadAsleacAsset("MegaLucario.png"));
        setAsleacTexture(getAsleacTexture().getScaledInstance(ScreenScaler.getScaledInt(128), ScreenScaler.getScaledInt(200), Image.SCALE_SMOOTH));
        getAsleac().setIcon(new ImageIcon(getAsleacTexture()));
        getAsleac().setBorder(BorderFactory.createEmptyBorder(0, ScreenScaler.getScaledInt(25), ScreenScaler.getScaledInt(210), 0));
        getAsleac().setAlignmentX(0.5f);
        getAsleac().setAlignmentY(0.5f);
        add(getAsleac());
    }


}
