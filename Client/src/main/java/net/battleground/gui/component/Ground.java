package net.battleground.gui.component;

import net.battleground.library.Dimension;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 17.03.2016.
 */
public abstract class Ground extends JLabel {

    private Image groundTexture;
    private Image asleacTexture;

    private JLabel asleac = new JLabel("");


    public Ground(Image icon) {
        this.groundTexture = icon;
        setIcon(new ImageIcon(icon));
        setLayout(new OverlayLayout(this));
        setHorizontalAlignment(CENTER);
    }

    public Image getGroundTexture() {
        return groundTexture;
    }

    public void setGroundTexture(Image groundTexture) {
        this.groundTexture = groundTexture;
        setIcon(new ImageIcon(groundTexture));
    }

    public Image getAsleacTexture() {
        return asleacTexture;
    }

    public void setAsleacTexture(Image asleacTexture) {
        this.asleacTexture = asleacTexture;
        asleac.setIcon(new ImageIcon(asleacTexture));
        setPreferredSize(new Dimension(128, 300));
    }

    public JLabel getAsleac() {
        return asleac;
    }

    public void setAsleac(JLabel asleac) {
        this.asleac = asleac;
    }
}
