package net.battleground.gui.component;

import net.battleground.entity.Asleac;
import net.battleground.factory.AssetFactory;
import net.battleground.library.Dimension;
import net.battleground.library.ScreenScaler;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 17.03.2016.
 */
public class AsleacGround extends Ground {

    public AsleacGround(Asleac asleac) {
        super(AssetFactory.loadImageAsset("EnemyGround.png").getScaledInstance(ScreenScaler.getScaledInt(64), ScreenScaler.getScaledInt(32), Image.SCALE_SMOOTH));
        setAsleacTexture(AssetFactory.loadAsleacAsset("Front" + asleac.getName() + ".png"));
        setAsleacTexture(getAsleacTexture().getScaledInstance(ScreenScaler.getScaledInt(77), ScreenScaler.getScaledInt(77), Image.SCALE_SMOOTH));
        getAsleac().setIcon(new ImageIcon(getAsleacTexture()));
        getAsleac().setBorder(BorderFactory.createEmptyBorder(0, 0, 70, 170));
        getAsleac().setAlignmentX(0.5f);

        setPreferredSize(new Dimension(64, 110));
        //setBorder(BorderFactory.createEmptyBorder(100,0,0,0));
        add(getAsleac());
    }

}
