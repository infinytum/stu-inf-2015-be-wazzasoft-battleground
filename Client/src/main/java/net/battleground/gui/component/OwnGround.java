package net.battleground.gui.component;

import net.battleground.entity.Asleac;
import net.battleground.factory.AssetFactory;
import net.battleground.library.ScreenScaler;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 17.03.2016.
 */
public class OwnGround extends Ground {
    public OwnGround(Asleac asleac) {
        super(AssetFactory.loadImageAsset("EnemyGround.png").getScaledInstance(ScreenScaler.getScaledInt(256), ScreenScaler.getScaledInt(128), Image.SCALE_SMOOTH));
        setAsleacTexture(AssetFactory.loadAsleacAsset("Back" + asleac.getName() + ".png"));
        setAsleacTexture(getAsleacTexture().getScaledInstance(ScreenScaler.getScaledInt(275), ScreenScaler.getScaledInt(275), Image.SCALE_SMOOTH));
        getAsleac().setIcon(new ImageIcon(getAsleacTexture()));
        getAsleac().setBorder(BorderFactory.createEmptyBorder(0, 0, 70, 0));
        getAsleac().setAlignmentX(0.5f);
        add(getAsleac());
    }
}
