package net.battleground.gui.screen;

import net.battleground.client.BattleClient;
import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventManager;
import net.battleground.factory.AssetFactory;
import net.battleground.gui.component.GlassPane;
import net.battleground.library.BackgroundPanel;
import net.battleground.library.Dimension;
import net.battleground.library.Font;
import net.battleground.library.ScreenScaler;
import net.battleground.listener.Listener;
import net.battleground.listener.PacketHandler;
import net.battleground.network.PacketManager;
import net.battleground.packet.RegisterStatusPacket;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bwaltm on 17.03.2016.
 */
public class RegisterScreen extends Screen {

    public RegisterScreen() {
        setTitle("Register");
        setContentPane(new BackgroundPanel(AssetFactory.loadImageAsset("LoginBackground.png")));
        setSize(600, 660);
        setMinimumSize(new Dimension(300, 655));

        //add create Back to Menu Button
        JButton backButton = new JButton("<= Back to menu");
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        GlassPane backPanel = new GlassPane();
        backPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        backPanel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(10), ScreenScaler.getScaledInt(10), 0, 0));
        backPanel.add(backButton);

        //create register Button
        JButton registerButton = new JButton("Register");
        registerButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        registerButton.setPreferredSize(new Dimension(100, 40));
        GlassPane registerPanel = new GlassPane();
        registerPanel.add(registerButton);

        //create the Label
        JLabel title1 = new JLabel("BATTLEGROUND");
        JLabel title2 = new JLabel("REGISTER");
        Font titleFont = new Font("Arial", Font.BOLD, 30);
        title1.setFont(titleFont);
        title2.setFont(titleFont);
        title1.setAlignmentX(Component.CENTER_ALIGNMENT);
        title2.setAlignmentX(Component.CENTER_ALIGNMENT);
        title1.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(40), 0, 0, 0));
        title2.setBorder(BorderFactory.createEmptyBorder(0, 0, ScreenScaler.getScaledInt(30), 0));

        //create nameInput Field
        JTextField usernameInput = new JTextField("username", 20);
        GlassPane usernamePanel = new GlassPane(); //Panel needed to reduce height
        usernamePanel.setMaximumSize(new Dimension(400, 40));
        usernamePanel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(5), 0, 0, 0));
        usernamePanel.add(usernameInput);

        //create emailInput Field
        JTextField emailField = new JTextField("e-mail", 20);
        GlassPane emailPanel = new GlassPane(); //Panel needed to reduce height
        usernamePanel.setMaximumSize(new Dimension(400, 40));
        emailPanel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(5), 0, 0, 0));
        emailPanel.add(emailField);


        //create passwordInput
        JPasswordField passwordField = new JPasswordField("********", 20);
        GlassPane passwordPanel = new GlassPane(); // panel needed to reduce height
        passwordPanel.setMaximumSize(new Dimension(400, 40));
        passwordPanel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(5), 0, 0, 0));
        passwordPanel.add(passwordField);

        //create repeatPassword
        JPasswordField repeatPasswordField = new JPasswordField("********", 20);
        GlassPane repeatPasswordPanel = new GlassPane(); // panel needed to reduce height
        repeatPasswordPanel.setMaximumSize(new Dimension(400, 40));
        repeatPasswordPanel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(5), 0, ScreenScaler.getScaledInt(30), 0));
        repeatPasswordPanel.add(repeatPasswordField);

        //Image
        JLabel ashBackLabel = new JLabel();
        Image ashBackImage = AssetFactory.loadImageAsset("AshBack.png");
        ashBackImage = ashBackImage.getScaledInstance(ScreenScaler.getScaledInt(130), ScreenScaler.getScaledInt(200), Image.SCALE_SMOOTH);
        Icon ashBackIcon = new ImageIcon(ashBackImage);
        ashBackLabel.setIcon(ashBackIcon);


        //contentLayout
        GlassPane contentPanel = new GlassPane();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));

        //credentials
        GlassPane credentialsGrid = new GlassPane();
        GlassPane credentialsPanel = new GlassPane();
        credentialsGrid.setLayout(new GridLayout(5, 1));
        credentialsGrid.setPreferredSize(new Dimension(400, 205));
        credentialsGrid.add(usernamePanel);
        credentialsGrid.add(emailPanel);
        credentialsGrid.add(passwordPanel);
        credentialsGrid.add(repeatPasswordPanel);
        credentialsGrid.add(registerPanel);
        credentialsPanel.add(credentialsGrid);


        //add components to content
        contentPanel.add(title1);
        contentPanel.add(title2);
        contentPanel.add(credentialsPanel);

        //add backButton and content to main page
        add(backPanel, BorderLayout.NORTH);
        add(contentPanel, BorderLayout.CENTER);
        add(ashBackLabel, BorderLayout.SOUTH);

        backButton.addActionListener(e -> {
            WelcomeScreen screen = new WelcomeScreen();
            screen.setVisible(true);
            EventManager.fireEvent(new ButtonClickEvent(backButton));
        });

        registerButton.addActionListener(e -> {
            EventManager.fireEvent(new ButtonClickEvent(registerButton));
            BattleClient.register(usernameInput.getText(), passwordField.getPassword(), repeatPasswordField.getPassword(), emailField.getText());
        });

        PacketManager.registerListener(new Listener() {

            @PacketHandler
            public void onRegisterStatusPacketReceive(RegisterStatusPacket packet) {
                if (!packet.isSuccess())
                    JOptionPane.showMessageDialog(Screen.getActiveScreen(),
                            packet.getReason(),
                            "Register Incorrect",
                            JOptionPane.ERROR_MESSAGE);
                else {
                    BattleClient.login(usernameInput.getText(), passwordField.getPassword());
                }
            }

        });
    }
}
