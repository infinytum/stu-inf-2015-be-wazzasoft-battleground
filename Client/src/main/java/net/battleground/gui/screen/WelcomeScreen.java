package net.battleground.gui.screen;

import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventManager;
import net.battleground.gui.component.MenuGround;
import net.battleground.library.BackgroundPanel;
import net.battleground.library.Dimension;
import net.battleground.library.Font;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 16.03.2016.
 */
public class WelcomeScreen extends Screen {

    public WelcomeScreen() {
        BackgroundPanel bgp = (BackgroundPanel) getContentPane();
        setLayout(new BorderLayout());
        setTitle("Welcome");

        BackgroundPanel panel = new BackgroundPanel(Color.WHITE);
        panel.setBackground(new Color(0, 0, 0, 0));
        panel.setOpaque(true);
        panel.setLayout(new BorderLayout());


        MenuGround ground = new MenuGround();
        ground.setText("Battleground");
        ground.setFont(new Font("Arial", 0, 30));
        ground.setHorizontalTextPosition(JLabel.CENTER);
        ground.setVerticalTextPosition(JLabel.BOTTOM);

        JButton login = new JButton("Login");
        login.setPreferredSize(new Dimension(100, 40));
        login.setFont(new Font("Arial", 0, 15));
        JButton register = new JButton("Register");
        register.setPreferredSize(new Dimension(100, 40));
        register.setFont(new Font("Arial", 0, 15));

        JPanel buttons = new JPanel();

        buttons.add(login);
        buttons.add(register);

        login.addActionListener(e -> {
            LoginScreen screen = new LoginScreen();
            screen.setVisible(true);
            EventManager.fireEvent(new ButtonClickEvent(login));
        });

        register.addActionListener(e -> {
            RegisterScreen screen = new RegisterScreen();
            screen.setVisible(true);
            EventManager.fireEvent(new ButtonClickEvent(register));
        });


        panel.add(ground, BorderLayout.CENTER);
        panel.add(buttons, BorderLayout.SOUTH);

        add(panel, BorderLayout.CENTER);
    }


}

