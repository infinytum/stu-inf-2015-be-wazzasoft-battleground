package net.battleground.gui.screen;

import net.battleground.factory.AssetFactory;
import net.battleground.library.BackgroundPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 16.03.2016.
 */
public abstract class Screen extends JFrame {

    private static Screen activeScreen;

    private String prefix = "Battleground - ";
    private String title = "Window";

    private Dimension size;//= new Dimension(400,600);

    private BackgroundPanel contentPane = new BackgroundPanel(AssetFactory.loadImageAsset("MenuBackground.png"));

    public Screen() {
        init();
    }

    public static Screen getActiveScreen() {
        return activeScreen;
    }

    private void init() {
        setLayout(new BorderLayout());
        Dimension a_fullscreen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        size = new Dimension((int) a_fullscreen.getWidth() / 2, (int) a_fullscreen.getHeight() / 2);

        super.setSize(size);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(contentPane);
        super.setTitle(prefix + title);
        setLocationRelativeTo(null);
    }

    @Override
    public String getTitle() {
        return super.getTitle();
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
        super.setTitle(prefix + title);
    }

    @Override
    public Dimension getSize() {
        return size;
    }

    @Override
    public void setSize(Dimension size) {
        this.size = size;
        super.setSize(size);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) {
            if (activeScreen != null)
                activeScreen.setVisible(false);
            activeScreen = this;
        }
    }
}
