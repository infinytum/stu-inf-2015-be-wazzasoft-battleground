package net.battleground.gui.screen;

import net.battleground.Position;
import net.battleground.client.BattleClient;
import net.battleground.entity.Asleac;
import net.battleground.factory.AssetFactory;
import net.battleground.gui.component.EnemyGround;
import net.battleground.gui.component.GlassPane;
import net.battleground.gui.component.OwnGround;
import net.battleground.gui.component.StatusBar;
import net.battleground.library.BackgroundPanel;
import net.battleground.library.Dimension;
import net.battleground.library.Font;
import net.battleground.library.ScreenScaler;
import net.battleground.listener.Listener;
import net.battleground.listener.PacketHandler;
import net.battleground.network.PacketManager;
import net.battleground.packet.BattleAttackPacket;
import net.battleground.packet.BattleInfoPacket;
import net.battleground.packet.BattleItemPacket;
import net.battleground.sound.Sound;
import net.battleground.sound.SoundManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by bteusm on 17.03.2016.
 */
public class BattleScreen extends Screen {

    public StatusBar enemyStatusBar;
    public EnemyGround enemyGround;

    private Asleac own;
    private Asleac enemy;
    private StatusBar ownStatusBar;
    private OwnGround ownGround;
    private GlassPane enemyPane = new GlassPane();
    private GlassPane ownPane = new GlassPane();

    private BackgroundPanel menu;
    private BackgroundPanel attacks;
    private BackgroundPanel items;
    private BackgroundPanel asleacs;

    private BackgroundPanel itemMenu;
    private JScrollPane itemsScrollBar;

    private String enemyName;


    private boolean waitingForPlayer2 = false;
    private boolean twoAttacks = false;


    public BattleScreen(Asleac own, Asleac enemy, String enemyName) {
        this.own = own;
        this.enemy = enemy;
        this.enemyName = enemyName;

        ownStatusBar = new StatusBar(own);
        enemyStatusBar = new StatusBar(enemy);

        ownGround = new OwnGround(own);
        enemyGround = new EnemyGround(enemy);

        setLayout(new BorderLayout());
        setSize(ScreenScaler.getScaledInt(800), ScreenScaler.getScaledInt(700));
        ((BackgroundPanel) getContentPane()).setImage(AssetFactory.loadImageAsset("BattleBackground.jpg"));

        GlassPane battleground = new GlassPane();
        battleground.setLayout(new GridLayout(0, 2));

        GlassPane enemyStatusPane = new GlassPane();
        enemyStatusPane.add(enemyStatusBar);

        enemyPane.setLayout(new BorderLayout());
        enemyGround.setVerticalAlignment(SwingConstants.BOTTOM);
        enemyPane.add(enemyGround, BorderLayout.SOUTH);
        enemyPane.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(70), 0, 0, 0));
        enemyGround.setBorder(BorderFactory.createEmptyBorder(0, ScreenScaler.getScaledInt(400), 0, 0));

        ownPane.setLayout(new BorderLayout());
        ownGround.setVerticalAlignment(SwingConstants.BOTTOM);
        ownPane.add(ownGround, BorderLayout.SOUTH);
        ownPane.setBorder(BorderFactory.createEmptyBorder(0, ScreenScaler.getScaledInt(100), 0, 0));

        GlassPane ownStatusPane = new GlassPane();
        ownStatusPane.setLayout(new BorderLayout());
        ownStatusBar.setVerticalAlignment(SwingConstants.BOTTOM);
        ownStatusPane.add(ownStatusBar, BorderLayout.SOUTH);
        ownStatusPane.setBorder(BorderFactory.createEmptyBorder(0, 0, ScreenScaler.getScaledInt(175), 0));

        battleground.add(enemyStatusPane);
        battleground.add(enemyPane);
        battleground.add(ownPane);
        battleground.add(ownStatusPane);

        java.awt.Dimension menuDimension = ScreenScaler.getScale(new java.awt.Dimension(800, 75));
        menu = new BackgroundPanel(AssetFactory.loadImageAsset("EmptyDialog.png").getScaledInstance((int) menuDimension.getWidth(), 150, Image.SCALE_SMOOTH));
        asleacs = new BackgroundPanel(AssetFactory.loadImageAsset("EmptyDialog.png").getScaledInstance(ScreenScaler.getScaledInt(800), 150, Image.SCALE_SMOOTH));
        attacks = new BackgroundPanel(AssetFactory.loadImageAsset("EmptyDialog.png").getScaledInstance(ScreenScaler.getScaledInt(800), 150, Image.SCALE_SMOOTH));
        items = new BackgroundPanel(AssetFactory.loadImageAsset("EmptyDialog.png").getScaledInstance(ScreenScaler.getScaledInt(800), 150, Image.SCALE_SMOOTH));

        itemMenu = new BackgroundPanel(AssetFactory.loadImageAsset("EmptyDialog.png").getScaledInstance(ScreenScaler.getScaledInt(800), 150, Image.SCALE_SMOOTH));


        menu.setLayout(new GridLayout(0, 2));
        asleacs.setLayout(new GridLayout(0, 2));
        attacks.setLayout(new GridLayout(0, 2));
        items.setLayout(new GridLayout(0, 2));

        GlassPane attackPanel = new GlassPane();
        JButton attack = new JButton("ATTACK");
        attackPanel.add(attack);
        attack.setOpaque(false);
        attack.setContentAreaFilled(false);
        attack.setBorderPainted(false);
        attack.setFont(new Font("Arial", Font.BOLD, 20));
        attack.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(19), 0, 0, 0));
        attack.setFocusPainted(false);

        GlassPane itemsPanel = new GlassPane();
        JButton items = new JButton("ITEMS");
        itemsPanel.add(items);
        items.setOpaque(false);
        items.setContentAreaFilled(false);
        items.setBorderPainted(false);
        items.setFont(new Font("Arial", Font.BOLD, 20));
        items.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(19), 0, 0, 0));
        items.setFocusPainted(false);

        GlassPane asleacskPanel = new GlassPane();
        JButton asleac = new JButton("ASLEAC");
        asleacskPanel.add(asleac);
        asleac.setOpaque(false);
        asleac.setContentAreaFilled(false);
        asleac.setBorderPainted(false);
        asleac.setFont(new Font("Arial", Font.BOLD, 20));
        asleac.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(19), 0, 0, 0));
        asleac.setFocusPainted(false);

        GlassPane surrenderPanel = new GlassPane();
        JButton surrender = new JButton("SURRENDER");
        surrenderPanel.add(surrender);
        surrender.setOpaque(false);
        surrender.setContentAreaFilled(false);
        surrender.setBorderPainted(false);
        surrender.setFont(new Font("Arial", Font.BOLD, 20));
        surrender.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(19), 0, 0, 0));
        surrender.setFocusPainted(false);

        menu.add(attackPanel);
        menu.add(itemsPanel);
        menu.add(asleacskPanel);
        menu.add(surrenderPanel);

        attack.addActionListener(e -> {
            SoundManager.playEffect(Sound.BUTTON_CLICK);
            hideMenu();
            showAttackMenu();
        });
        surrender.addActionListener(e -> {
            SoundManager.playEffect(Sound.BUTTON_CLICK);
            BattleClient.sendPacket(new BattleInfoPacket(BattleInfoPacket.BattleState.SURRENDER));
            SoundManager.stopSound();
        });
        asleac.addActionListener(e -> SoundManager.playEffect(Sound.BUTTON_CLICK));
        items.addActionListener(e -> {
            SoundManager.playEffect(Sound.BUTTON_CLICK);
            hideMenu();
            showItemMenu();
        });

        add(battleground, BorderLayout.CENTER);

        // Attacks Menu

        for (int i = 0; i < own.getAbilities().size(); i++) {
            GlassPane abilityPanel = new GlassPane();
            JButton ability = new JButton(own.getAbilities().get(i).getName());
            abilityPanel.add(ability);
            ability.setOpaque(false);
            ability.setContentAreaFilled(false);
            ability.setBorderPainted(false);
            ability.setFont(new Font("Arial", Font.BOLD, 20));
            ability.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(19), 0, 0, 0));
            ability.setFocusPainted(false);
            final int finalli = i;
            ability.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    boolean abilitySucess = own.getAbilities().get(finalli).use(enemy, own);
                    if (abilitySucess) {
                        Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            int i = 0;

                            @Override
                            public void run() {
                                if (i == 9)
                                    timer.cancel();
                                i++;
                                enemyGround.getAsleac().setVisible(!enemyGround.getAsleac().isVisible());
                            }
                        }, 0, 100);
                        //TODO: Display message
                        BattleClient.attack(own.getAbilities().get(finalli));
                        if (!twoAttacks) {
                            hideAttackMenu();
                            waitingForPlayer2 = true;
                        } else {
                            twoAttacks = false;
                            hideAttackMenu();
                            showMenu();
                        }
                    } else {
                        hideAttackMenu();
                        displayMessage("You cant use this ability!");
                    }
                }
            });
            attacks.add(abilityPanel);
        }

        GlassPane itemList = new GlassPane();
        itemList.setLayout(new BoxLayout(itemList, BoxLayout.Y_AXIS));
        itemList.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        itemsScrollBar = new JScrollPane(itemList,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        itemsScrollBar.setBorder(null);
        itemsScrollBar.setOpaque(false);
        itemsScrollBar.getViewport().setOpaque(false);

        for (Position p : BattleClient.getPlayer().getItems()) {
            GlassPane item = new GlassPane();
            item.setLayout(new GridLayout(1, 2));
            item.setPreferredSize(new Dimension(this.getWidth(), 30));

            JLabel itemName = new JLabel(p.getItem().getName());
            itemName.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
            itemName.setFont(new Font("Arial", 0, 20));

            itemName.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (p.getAmount() == 0)
                        return;
                    p.getItem().use(own);
                    p.setAmount(p.getAmount() - 1);
                    BattleClient.sendPacket(new BattleItemPacket(p.getItem()));
                    hideItemMenu();
                    waitingForPlayer2 = true;
                    twoAttacks = true;
                }
            });

            JLabel itemAmount = new JLabel(p.getAmount() + "X");
            itemAmount.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 30));
            itemAmount.setFont(new Font("Arial", 0, 20));
            item.add(itemName);
            item.add(itemAmount);
            itemList.add(item);
        }
        itemMenu.add(itemsScrollBar);

        getRootPane().getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                "doSomething");
        getRootPane().getActionMap().put("doSomething",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (waitingForPlayer2)
                            return;
                        hideAttackMenu();
                        hideItemMenu();
                        showMenu();
                    }
                });

        final Listener attackListener = new Listener() {

            @PacketHandler
            public void onBattleAttackPacketReceive(BattleAttackPacket packet) {
                if (packet.getUsedAbility().use(own, enemy)) {
                    SoundManager.playEffect(Sound.HIT);
                    if (own.isAlive()) {
                        waitingForPlayer2 = false;
                        Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            int i = 0;

                            @Override
                            public void run() {
                                if (i == 9)
                                    timer.cancel();
                                i++;
                                ownGround.getAsleac().setVisible(!ownGround.getAsleac().isVisible());
                            }
                        }, 0, 100);

                        if (twoAttacks) {
                            twoAttacks = false;
                            return;
                        }

                        showMenu();
                    } else {
                        BattleInfoPacket battleInfoPacket = new BattleInfoPacket(BattleInfoPacket.BattleState.DIE);
                        BattleClient.sendPacket(battleInfoPacket);
                    }
                } else {
                }
            }

        };

        final Listener battleInfoListener = new Listener() {

            @PacketHandler
            public void onBattleInfoPacketReceive(BattleInfoPacket packet) {
                Listener listener = this;
                if (packet.getBattleState() == BattleInfoPacket.BattleState.END) {
                    hideMenu();
                    SoundManager.stopSound();

                    GlassPane messagePanel = new GlassPane();
                    JLabel message = new JLabel(packet.getWinner() + " WON THE BATTLE!");
                    BackgroundPanel p = new BackgroundPanel(AssetFactory.loadImageAsset("EmptyDialog.png").getScaledInstance(ScreenScaler.getScaledInt(800), ScreenScaler.getScaledInt(150), Image.SCALE_SMOOTH));

                    messagePanel.add(message);
                    message.setOpaque(false);
                    message.setFont(new Font("Arial", Font.BOLD, 20));
                    message.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(19), 0, 0, 0));
                    message.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            MainMenuScreen screen = new MainMenuScreen();
                            screen.setVisible(true);
                            PacketManager.unregisterListener(attackListener);
                            PacketManager.unregisterListener(listener);
                            SoundManager.stopSound();
                        }
                    });


                    p.add(messagePanel);
                    add(p, BorderLayout.SOUTH);
                    repaint();
                    validate();
                }
            }

        };

        final Listener battleItemListener = new Listener() {

            @PacketHandler
            public void onBattleItemPacketReceive(BattleItemPacket packet) {
                packet.getItem().use(enemy);
                displayMessage(enemyName + " USED " + packet.getItem().getName());
                twoAttacks = true;
            }

        };

        PacketManager.registerListener(attackListener);

        PacketManager.registerListener(battleItemListener);

        PacketManager.registerListener(battleInfoListener);

        displayMessage(enemyName.toUpperCase() + " WANTS TO FIGHT!");

    }

    public void showMenu() {
        add(menu, BorderLayout.SOUTH);
        repaint();
        validate();
    }


    public void hideMenu() {
        remove(menu);
        repaint();
        validate();
    }

    public void showItemMenu() {
        add(itemMenu, BorderLayout.SOUTH);
        repaint();
        validate();
    }

    public void hideItemMenu() {
        remove(itemMenu);
        repaint();
        validate();
    }

    public void showAttackMenu() {
        add(attacks, BorderLayout.SOUTH);
        repaint();
        validate();
    }

    public void hideAttackMenu() {
        remove(attacks);
        repaint();
        validate();
    }

    public void displayMessage(String s) {
        hideMenu();
        hideAttackMenu();
        hideItemMenu();

        GlassPane messagePanel = new GlassPane();
        JLabel message = new JLabel(s);
        BackgroundPanel p = new BackgroundPanel(AssetFactory.loadImageAsset("EmptyDialog.png").getScaledInstance(ScreenScaler.getScaledInt(800), ScreenScaler.getScaledInt(150), Image.SCALE_SMOOTH));

        messagePanel.add(message);
        message.setOpaque(false);
        message.setFont(new Font("Arial", Font.BOLD, 20));
        message.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(19), 0, 0, 0));
        message.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                remove(p);
                showMenu();
                repaint();
                validate();
            }
        });


        p.add(messagePanel);
        add(p, BorderLayout.SOUTH);
        repaint();
        validate();
    }


    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        java.util.Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (int i = 400; i >= 0; i--) {
                    enemyGround.setBorder(BorderFactory.createEmptyBorder(0, i, 0, 0));
                    try {
                        Thread.sleep(6);
                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                    }
                }
            }
        }, 0);
    }


}
