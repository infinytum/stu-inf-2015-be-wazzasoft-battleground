package net.battleground.gui.screen;

import net.battleground.Position;
import net.battleground.client.BattleClient;
import net.battleground.entity.Asleac;
import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventManager;
import net.battleground.gui.component.*;
import net.battleground.library.WrapLayout;
import net.battleground.listener.Listener;
import net.battleground.listener.PacketHandler;
import net.battleground.network.PacketManager;
import net.battleground.packet.BuyItemRequestPacket;
import net.battleground.packet.ShopRequestPacket;
import net.battleground.packet.ShopResponsePacket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by bteusm on 11.03.2016.
 */
public class ShopScreen extends Screen {

    public int asleacCount = 0;
    ShopScreen shopScreen;
    private int money = 0;
    private GlassPane asleacList = new GlassPane();
    //backButton
    private JButton backButton = new JButton("<= back to Menu");
    //moneyLabel
    private JLabel moneyLabel = new JLabel("Money: " + money + "$");

    public ShopScreen() {
        shopScreen = this;
        setSize(new Dimension(650, 900));
        setMinimumSize(new Dimension(600, 500));

        GlassPane content = new GlassPane();
        content.setLayout(new BorderLayout());

        GlassPane titlePanel = new GlassPane();
        titlePanel.setLayout(new WrapLayout());

        //pageTitle
        JLabel title = new JLabel("SHOP");
        title.setFont(new Font("Arial", Font.BOLD, 30));
        title.setBorder(BorderFactory.createEmptyBorder(0, 100, 0, 0));

        //MoneyLabel
        moneyLabel.setFont(new Font("Arial", Font.PLAIN, 18));
        moneyLabel.setBorder(BorderFactory.createEmptyBorder(0, 100, 0, 0));

        //add back title and money
        titlePanel.add(backButton);
        titlePanel.add(title);
        titlePanel.add(moneyLabel);

        content.add(titlePanel, BorderLayout.NORTH);

        JSplitPane page = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        page.setBorder(null);

        //generate asleacsPanel
        GlassPane asleacs = new GlassPane();
        asleacs.setLayout(new BorderLayout());
        asleacs.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));


        //generate Title and moneyLabel
        JLabel asleacsTitle = new JLabel("Your Asleac", SwingConstants.CENTER);
        asleacsTitle.setFont(new Font("Arial", Font.PLAIN, 25));
        asleacsTitle.setBorder(BorderFactory.createEmptyBorder(0, 100, 0, 0));

        asleacs.add(asleacsTitle, BorderLayout.CENTER);

        JScrollPane asleacScrollBar = new JScrollPane(asleacs,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        asleacScrollBar.setBorder(null);
        asleacScrollBar.setOpaque(false);
        asleacScrollBar.getViewport().setOpaque(false);

        //generate itemsPanel
        GlassPane items = new GlassPane();
        items.setLayout(new BorderLayout());

        //itemsTitle
        JLabel itemsTitle = new JLabel("Items & Asleac", SwingConstants.CENTER);
        itemsTitle.setFont(new Font("Arial", Font.PLAIN, 25));
        itemsTitle.setBorder(BorderFactory.createEmptyBorder(0, 100, 0, 0));

        items.add(itemsTitle, BorderLayout.NORTH);

        JScrollPane itemsScrollBar = new JScrollPane(items,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        itemsScrollBar.setBorder(null);
        itemsScrollBar.setOpaque(false);
        itemsScrollBar.getViewport().setOpaque(false);


        //add components to splitscreen
        page.setTopComponent(asleacScrollBar);
        page.setBottomComponent(itemsScrollBar);
        page.setOpaque(false);

        content.add(page, BorderLayout.CENTER);
        add(content);

        ShopScreen shopScreen = this;
        PacketManager.registerListener(new Listener() {
            @PacketHandler
            public void onShopResponsePacketReceive(ShopResponsePacket packet) {
                money = packet.getMoney();
                moneyLabel.setText("Money: " + money + "$");

                //add owned asleacs

                asleacList.setLayout(new WrapLayout());
                asleacCount = packet.getOwnedAsleacs().size();
                for (Asleac ownedAsleac : packet.getOwnedAsleacs()) {
                    addOwnedAsleac(ownedAsleac);
                }
                asleacs.add(asleacList, BorderLayout.SOUTH);


                GlassPane itemsList = new GlassPane();
                itemsList.setLayout(new WrapLayout());

                //add buyable asleacs to itemList
                for (Asleac buyAsleac : packet.getBuyableAsleacs()) {
                    ShopAsleac asleacItem = new ShopAsleac(buyAsleac);
                    asleacItem.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            super.mouseClicked(e);

                            if (asleacCount < 6) {
                                if (buyAsleac.getPrice() <= money) {
                                    new BuyAsleac(shopScreen, buyAsleac);
                                } else {
                                    JOptionPane.showMessageDialog(shopScreen,
                                            "You dont have enough money",
                                            "Warning",
                                            JOptionPane.WARNING_MESSAGE);
                                }
                            } else {
                                JOptionPane.showMessageDialog(shopScreen,
                                        "You cant have more than 6 Asleac",
                                        "Warning",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    });
                    itemsList.add(asleacItem);
                }

                //add items to List
                for (Position position : packet.getItems()) {
                    ShopItem shopItem = new ShopItem(position);
                    shopItem.addMouseListener(new MouseAdapter() {
                        // add mouseListener to buy items and actualize labels
                        public void mouseClicked(MouseEvent e) {
                            int price = position.getItem().getPrice();
                            if (money >= price) {
                                money = money - price;
                                BattleClient.sendPacket(new BuyItemRequestPacket(position.getItem().getId()));
                                moneyLabel.setText("Money: " + money + "$");
                                shopItem.setAmount(shopItem.getAmount() + 1);
                            } else {
                                JOptionPane.showMessageDialog(shopScreen,
                                        "You dont have enough Money to buy this potion",
                                        "Warning",
                                        JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    });
                    itemsList.add(shopItem);
                }
                items.add(itemsList, BorderLayout.CENTER);

                //end by repainting everything
                page.resetToPreferredSizes();
                shopScreen.repaint();
                shopScreen.validate();
                PacketManager.unregisterListener(this);
            }
        });


        backButton.addActionListener(e -> {
            MainMenuScreen screen = new MainMenuScreen();
            screen.setVisible(true);
            EventManager.fireEvent(new ButtonClickEvent(backButton));
            dispose();
        });
        BattleClient.sendPacket(new ShopRequestPacket());
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
        moneyLabel.setText("Money: " + money + "$");
    }

    public void addOwnedAsleac(Asleac ownedAsleac) {
        ShopAsleac ownedShopAsleac = new ShopAsleac(ownedAsleac);

        ownedShopAsleac.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (asleacCount == 1) {
                    JOptionPane.showMessageDialog(shopScreen,
                            "You cant sell your last Asleac",
                            "Warning",
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    new SellAsleac(shopScreen, ownedAsleac, ownedShopAsleac);
                }
            }
        });

        asleacList.add(ownedShopAsleac);
        repaint();
    }
}
