package net.battleground.gui.screen;

import net.battleground.client.BattleClient;
import net.battleground.entity.Asleac;
import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventManager;
import net.battleground.gui.component.AsleacGround;
import net.battleground.gui.component.GlassPane;
import net.battleground.gui.component.StatusBar;
import net.battleground.library.Dimension;
import net.battleground.library.ScreenScaler;
import net.battleground.listener.Listener;
import net.battleground.listener.PacketHandler;
import net.battleground.network.PacketManager;
import net.battleground.packet.AsleacRequestPacket;
import net.battleground.packet.AsleacResponsePacket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by bteusm on 24.03.2016.
 */
public class AsleacScreen extends Screen {

    public AsleacScreen() {
        setTitle("Asleacs");
        setLayout(new BorderLayout());
        setSize(new net.battleground.library.Dimension(600, 700));

        GlassPane north = new GlassPane();
        JButton backButton = new JButton("<--");
        backButton.setHorizontalAlignment(SwingConstants.LEFT);

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventManager.fireEvent(new ButtonClickEvent(backButton));
                MainMenuScreen screen = new MainMenuScreen();
                screen.setVisible(true);
            }
        });

        north.add(backButton);
        add(north, BorderLayout.NORTH);


        GlassPane center = new GlassPane();
        center.setLayout(new GridLayout(6, 1));

        // Querying server for data...

        PacketManager.registerListener(new Listener() {

            @PacketHandler
            public void onAsleacResponsePacketReceive(AsleacResponsePacket packet) {


                for (Asleac test : packet.getAsleacs()) {

                    AsleacGround aground = new AsleacGround(test);
                    GlassPane pokemon = new GlassPane();
                    pokemon.setLayout(new BorderLayout());

                    GlassPane ground = new GlassPane();
                    ground.setPreferredSize(new Dimension(228, 220));

                    GlassPane status = new GlassPane();

                    StatusBar bar = new StatusBar(test);

                    status.add(bar);


                    status.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(25), 0, 0, 0));
                    ground.setBorder(BorderFactory.createEmptyBorder(0, 0, 140, 0));

                    ground.add(aground);

                    pokemon.add(ground, BorderLayout.WEST);
                    pokemon.add(status, BorderLayout.CENTER);
                    center.add(pokemon);
                    repaint();
                    validate();
                }

                PacketManager.unregisterListener(this);
            }

        });

        BattleClient.sendPacket(new AsleacRequestPacket());


        add(center);


    }


}
