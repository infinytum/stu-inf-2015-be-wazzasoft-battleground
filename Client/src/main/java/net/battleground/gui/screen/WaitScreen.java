package net.battleground.gui.screen;

import net.battleground.client.BattleClient;
import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventManager;
import net.battleground.gui.component.GlassPane;
import net.battleground.library.Font;
import net.battleground.listener.Listener;
import net.battleground.listener.PacketHandler;
import net.battleground.network.PacketManager;
import net.battleground.packet.BattleInfoPacket;
import net.battleground.sound.Sound;
import net.battleground.sound.SoundManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by bteusm on 24.03.2016.
 */
public class WaitScreen extends Screen {

    public WaitScreen() {
        setTitle("Waiting for Player...");
        setLayout(new BorderLayout());

        GlassPane north = new GlassPane();
        JButton backButton = new JButton("Abort");

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EventManager.fireEvent(new ButtonClickEvent(backButton));
                BattleClient.sendPacket(new BattleInfoPacket(BattleInfoPacket.BattleState.QUIT_WAIT));
                MainMenuScreen screen = new MainMenuScreen();
                screen.setVisible(true);
            }
        });

        north.add(backButton);
        add(north, BorderLayout.NORTH);

        GlassPane center = new GlassPane();
        JLabel waitingLabel = new JLabel("WAITING FOR OTHER PLAYER...", JLabel.CENTER);
        waitingLabel.setFont(new Font("Arial", 0, 20));
        center.add(waitingLabel);
        add(center, BorderLayout.CENTER);


        PacketManager.registerListener(new Listener() {

            @PacketHandler
            public void onPacketReceive(BattleInfoPacket packet) {
                if (packet.getBattleState() == BattleInfoPacket.BattleState.BEGIN) {
                    BattleScreen screen = new BattleScreen(packet.getOwnAsleac(), packet.getEnemyAsleac(), packet.getEnemyName());
                    screen.setVisible(true);
                    SoundManager.playSound(Sound.BATTLE_INTRO);
                    PacketManager.unregisterListener(this);
                }
                if (packet.getBattleState() == BattleInfoPacket.BattleState.QUIT_WAIT) {
                    PacketManager.unregisterListener(this);
                }
            }

        });

        BattleClient.sendPacket(new BattleInfoPacket(BattleInfoPacket.BattleState.WAITING));
    }

}
