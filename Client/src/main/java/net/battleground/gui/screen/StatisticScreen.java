package net.battleground.gui.screen;

import net.battleground.client.BattleClient;
import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventManager;
import net.battleground.gui.component.GlassPane;
import net.battleground.library.ScreenScaler;
import net.battleground.listener.Listener;
import net.battleground.listener.PacketHandler;
import net.battleground.network.PacketManager;
import net.battleground.packet.StatisticRequestPacket;
import net.battleground.packet.StatisticResponsePacket;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bteusm on 23.03.2016.
 */
public class StatisticScreen extends Screen {

    private JLabel playedGames = new JLabel("Played Games: ", JLabel.CENTER);
    private JLabel wonGames = new JLabel("Won Games: ", JLabel.CENTER);
    private JLabel asleacsPossessed = new JLabel("Asleacs Possessed: ", JLabel.CENTER);
    private JLabel money = new JLabel("Money: ", JLabel.CENTER);

    public StatisticScreen() {
        setTitle("Statistics");
        setLayout(new BorderLayout());


        JButton backButton = new JButton("<= back to Menu");
        GlassPane backPanel = new GlassPane();
        backPanel.add(backButton);
        backPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        backPanel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(10), ScreenScaler.getScaledInt(10), 0, 0));
        add(backPanel, BorderLayout.NORTH);


        GlassPane statisticPanel = new GlassPane();
        statisticPanel.setLayout(new GridLayout(0, 1));

        statisticPanel.add(playedGames);
        statisticPanel.add(wonGames);
        statisticPanel.add(asleacsPossessed);
        statisticPanel.add(money);

        add(statisticPanel, BorderLayout.CENTER);

        PacketManager.registerListener(new Listener() {
            @PacketHandler
            public void onStatisticResponsePacketReceive(StatisticResponsePacket packet) {
                playedGames.setText("Played Games: " + packet.getGames());
                wonGames.setText("Won Games: " + packet.getWins());
                money.setText("Money: " + packet.getMoney());
                asleacsPossessed.setText("Asleacs Possessed: " + packet.getAsleacs_possessed());
            }
        });

        backButton.addActionListener(e -> {
            MainMenuScreen screen = new MainMenuScreen();
            screen.setVisible(true);
            EventManager.fireEvent(new ButtonClickEvent(backButton));
        });

        BattleClient.sendPacket(new StatisticRequestPacket());

    }
}

