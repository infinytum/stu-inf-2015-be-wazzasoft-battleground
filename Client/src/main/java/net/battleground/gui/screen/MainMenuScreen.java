package net.battleground.gui.screen;

import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventManager;
import net.battleground.factory.AssetFactory;
import net.battleground.gui.component.GlassPane;
import net.battleground.library.BackgroundPanel;
import net.battleground.library.Dimension;
import net.battleground.library.Font;
import net.battleground.library.ScreenScaler;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bwaltm on 17.03.2016.
 */
public class MainMenuScreen extends Screen {

    public MainMenuScreen() {
        setTitle("Main Menu");
        setContentPane(new BackgroundPanel(AssetFactory.loadImageAsset("LoginBackground.png")));
        setMinimumSize(new Dimension(300, 655));
        setLayout(new GridLayout(6, 1));

        JLabel title = new JLabel("Main Menu", SwingConstants.CENTER);
        title.setFont(new Font("Arial", Font.BOLD, 50));
        title.setForeground(Color.red);

        JPanel playPanel = ButtonPanel("PLAY");
        JPanel shopPanel = ButtonPanel("SHOP");
        JPanel asleacPanel = ButtonPanel("ASLEAC");
        JPanel statsPanel = ButtonPanel("STATS");
        JPanel logoutPanel = ButtonPanel("LOGOUT");

        add(title);
        add(playPanel);
        add(shopPanel);
        add(asleacPanel);
        add(statsPanel);
        add(logoutPanel);

        JButton playButton = (JButton) playPanel.getComponent(0);
        JButton shopButton = (JButton) shopPanel.getComponent(0);
        JButton asleacButton = (JButton) asleacPanel.getComponent(0);
        JButton statsButton = (JButton) statsPanel.getComponent(0);
        JButton logoutButton = (JButton) logoutPanel.getComponent(0);

        playButton.addActionListener(e -> {
            EventManager.fireEvent(new ButtonClickEvent(playButton));
            WaitScreen screen = new WaitScreen();
            screen.setVisible(true);
        });

        shopButton.addActionListener(e -> {
            EventManager.fireEvent(new ButtonClickEvent(shopButton));
            ShopScreen screen = new ShopScreen();
            screen.setVisible(true);
        });

        asleacButton.addActionListener(e -> {
            EventManager.fireEvent(new ButtonClickEvent(asleacButton));
            AsleacScreen screen = new AsleacScreen();
            screen.setVisible(true);
        });

        statsButton.addActionListener(e -> {
            EventManager.fireEvent(new ButtonClickEvent(statsButton));
            StatisticScreen screen = new StatisticScreen();
            screen.setVisible(true);
        });

        logoutButton.addActionListener(e -> {
            EventManager.fireEvent(new ButtonClickEvent(logoutButton));
            System.exit(0);
        });


    }

    private GlassPane ButtonPanel(String buttonText) {
        GlassPane panel = new GlassPane();
        JButton button = new JButton(buttonText);
        button.setPreferredSize(new Dimension(200, 45));

        panel.add(button);
        panel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(35), 0, 0, 0));

        return panel;
    }
}
