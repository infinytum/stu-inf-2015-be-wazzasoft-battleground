package net.battleground.gui.screen;

import net.battleground.client.BattleClient;
import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventManager;
import net.battleground.factory.AssetFactory;
import net.battleground.gui.component.GlassPane;
import net.battleground.library.BackgroundPanel;
import net.battleground.library.Dimension;
import net.battleground.library.Font;
import net.battleground.library.ScreenScaler;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bwaltm on 16.03.2016.
 */
public class LoginScreen extends Screen {

    public LoginScreen() {

        setTitle("Login");
        setContentPane(new BackgroundPanel(AssetFactory.loadImageAsset("LoginBackground.png")));
        setSize(ScreenScaler.getScaledInt(600), ScreenScaler.getScaledInt(660));
        setMinimumSize(new Dimension(300, 655));

        //add create Back to Menu Button
        JButton backButton = new JButton("<= Back to menu");
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        GlassPane backPanel = new GlassPane();
        backPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        backPanel.setBorder(BorderFactory.createEmptyBorder(ScreenScaler.getScaledInt(10), ScreenScaler.getScaledInt(10), 0, 0));
        backPanel.add(backButton);

        //create login Button
        JButton loginButton = new JButton("Login");
        loginButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginButton.setPreferredSize(new Dimension(100, 40));
        loginButton.setFont(new Font("Arial", 0, 15));
        GlassPane loginPanel = new GlassPane();
        loginPanel.add(loginButton);

        //create the title
        JLabel title1 = new JLabel("BATTLEGROUND");
        JLabel title2 = new JLabel("LOGIN");
        Font titleFont = new Font("Arial", Font.BOLD, 30);
        title1.setFont(titleFont);
        title2.setFont(titleFont);
        title1.setAlignmentX(Component.CENTER_ALIGNMENT);
        title2.setAlignmentX(Component.CENTER_ALIGNMENT);
        title1.setBorder(BorderFactory.createEmptyBorder(40, 0, 0, 0));
        title2.setBorder(BorderFactory.createEmptyBorder(0, 0, 30, 0));

        //create nameInput Field
        JTextField usernameInput = new JTextField("username", 20);
        usernameInput.setFont(new Font("Arial", 0, 15));
        usernameInput.setPreferredSize(new Dimension(400, 20));
        GlassPane usernamePanel = new GlassPane(); //Panel needed to reduce height
        usernamePanel.setMaximumSize(new Dimension(400, 20));
        usernamePanel.add(usernameInput);

        //create passwordInput
        JPasswordField passwordField = new JPasswordField("********", 20);
        passwordField.setFont(new Font("Arial", 0, 15));
        passwordField.setPreferredSize(new Dimension(400, 20));
        GlassPane passwordPanel = new GlassPane(); // panel needed to reduce height
        passwordPanel.setMaximumSize(ScreenScaler.getScale(new Dimension(400, 20)));
        passwordPanel.setMinimumSize(ScreenScaler.getScale(new Dimension(400, 20)));
        passwordPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 30, 0));
        passwordPanel.add(passwordField);

        //Image
        JLabel ashBackLabel = new JLabel();
        Image ashBackImage = AssetFactory.loadImageAsset("AshBack.png");
        ashBackImage = ashBackImage.getScaledInstance(ScreenScaler.getScaledInt(130), ScreenScaler.getScaledInt(200), Image.SCALE_SMOOTH);
        Icon ashBackIcon = new ImageIcon(ashBackImage);
        ashBackLabel.setIcon(ashBackIcon);


        //contentLayout
        GlassPane contentPanel = new GlassPane();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));

        //add components to content
        contentPanel.add(title1);
        contentPanel.add(title2);
        contentPanel.add(usernamePanel);
        contentPanel.add(passwordPanel);
        contentPanel.add(loginPanel);

        //add backButton and content to main page
        add(backPanel, BorderLayout.NORTH);
        add(contentPanel, BorderLayout.CENTER);
        add(ashBackLabel, BorderLayout.SOUTH);

        backButton.addActionListener(e -> {
            WelcomeScreen screen = new WelcomeScreen();
            screen.setVisible(true);
            EventManager.fireEvent(new ButtonClickEvent(backButton));
        });

        loginButton.addActionListener(e -> {
            EventManager.fireEvent(new ButtonClickEvent(loginButton));
            BattleClient.login(usernameInput.getText(), passwordField.getPassword());
        });
    }
}
