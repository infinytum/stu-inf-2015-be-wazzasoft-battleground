package net.battleground.listener;

import net.battleground.event.ButtonClickEvent;
import net.battleground.event.EventHandler;
import net.battleground.event.SoundStopEvent;
import net.battleground.sound.Sound;
import net.battleground.sound.SoundManager;

/**
 * Created by mkteu on 18.03.2016.
 */
public class SoundListener implements Listener {

    @EventHandler
    public void onSoundStop(SoundStopEvent e) {
        if (e.getSound() == Sound.BATTLE_INTRO)
            SoundManager.playSound(Sound.BATTLE_MAIN);
        if (e.getSound() == Sound.BATTLE_MAIN)
            SoundManager.playSound(Sound.BATTLE_MAIN);
    }

    @EventHandler
    public void onButtonClick(ButtonClickEvent e) {
        SoundManager.playEffect(Sound.BUTTON_CLICK);
    }


}
