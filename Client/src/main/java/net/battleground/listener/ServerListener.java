package net.battleground.listener;

import net.battleground.client.BattleClient;
import net.battleground.event.EventHandler;
import net.battleground.event.ServerConnectedEvent;
import net.battleground.gui.component.BuyAsleac;
import net.battleground.gui.screen.MainMenuScreen;
import net.battleground.gui.screen.Screen;
import net.battleground.packet.HandshakePacket;
import net.battleground.packet.LoginStatusPacket;
import net.battleground.packet.PlayerUpdatePacket;

import javax.swing.*;

/**
 * Created by bteusm on 18.03.2016.
 */
public class ServerListener implements Listener {


    @EventHandler
    public void onServerConnect(ServerConnectedEvent e) {
    }

    @PacketHandler
    public void onHandshake(HandshakePacket packet) throws Exception {
        BattleClient.getBattleClient().setClientID(packet.getClientID());
    }

    @PacketHandler
    public void onPlayerUpdate(PlayerUpdatePacket packet) {
        BattleClient.getBattleClient().setPlayer(packet.getPlayer());
    }

    @PacketHandler
    public void onLoginStateReceive(LoginStatusPacket packet) {
        if (packet.isSuccess()) {
            BattleClient.getBattleClient().setPlayer(packet.getPlayer());
            MainMenuScreen screen = new MainMenuScreen();
            screen.setVisible(true);
            if (packet.getPlayer().getAsleacs().size() == 0) {
                BuyAsleac buyAsleac = new BuyAsleac(Screen.getActiveScreen());
            }
        } else {
            JOptionPane.showMessageDialog(Screen.getActiveScreen(),
                    "You have entered wrong credentials!",
                    "Login Incorrect",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
