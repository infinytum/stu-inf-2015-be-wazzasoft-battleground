package net.battleground.event;

import net.battleground.sound.Sound;

/**
 * Created by mkteu on 18.03.2016.
 */
public class SoundStartEvent extends Event {

    private Sound sound;

    public SoundStartEvent(Sound sound) {
        this.sound = sound;
    }

    public Sound getSound() {
        return this.sound;
    }

}
