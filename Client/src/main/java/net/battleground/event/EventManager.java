package net.battleground.event;

import net.battleground.Logger;
import net.battleground.listener.Listener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mkteu on 18.03.2016.
 */
public abstract class EventManager {

    private static HashMap<Class<? extends Event>, ArrayList<Method>> registeredListener = new HashMap<>();
    private static HashMap<Method, Listener> listenerMap = new HashMap<>();


    /**
     * Invokes all methods registered on the given event, and passes it to them
     *
     * @param event The event you want to fire
     */
    public static void fireEvent(Event event) {
        Logger.info("Firing event " + event.getClass().getSimpleName());
        try {
            if (!registeredListener.containsKey(event.getClass()))  //Check if we have some registered handlers
                return;

            for (Method m : registeredListener.get(event.getClass())) {     // Loop through all methods and invoke them with the given event
                m.invoke(listenerMap.get(m), event);
            }

        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        }
    }


    public static void registerListener(Listener listener) {
        for (Method m : listener.getClass().getDeclaredMethods()) { // Loop through all Methods declared in the class
            if (m.isAnnotationPresent(EventHandler.class)) {    // Check if method has the @EventHandler Annotation
                Class<? extends Event> clazz = m.getParameterTypes()[0].asSubclass(Event.class);    // Get which event the method would like to receive
                registerMethod(m, clazz);   // Register it in the map
                listenerMap.put(m, listener);   // Save the listener instance so we can use it later
            }
        }
    }

    private static void registerMethod(Method m, Class<? extends Event> clazz) {
        ArrayList<Method> listener = new ArrayList<>();
        if (registeredListener.containsKey(clazz))  // If there are already some registered methods, load the array
            listener = registeredListener.get(clazz);

        listener.add(m);    // Add the new method
        registeredListener.remove(clazz);   // Remove the old array
        registeredListener.put(clazz, listener); // Save the new array
    }


}
