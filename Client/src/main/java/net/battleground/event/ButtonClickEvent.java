package net.battleground.event;

import javax.swing.*;

/**
 * Created by bteusm on 18.03.2016.
 */
public class ButtonClickEvent extends Event {

    private JButton button;

    public ButtonClickEvent(JButton button) {
        this.button = button;
    }

    public JButton getButton() {
        return button;
    }
}
