package net.battleground.event;

import net.battleground.sound.Sound;

/**
 * Created by mkteu on 18.03.2016.
 */
public class SoundStopEvent extends Event {

    private Sound sound;

    public SoundStopEvent(Sound sound) {
        this.sound = sound;
    }

    public Sound getSound() {
        return this.sound;
    }

}
