package net.battleground.sound;

import net.battleground.Logger;
import net.battleground.event.EventManager;
import net.battleground.event.SoundStartEvent;
import net.battleground.event.SoundStopEvent;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Michael on 17.03.2016.
 */
public abstract class SoundManager {

    private static InputStream bufferedIn;
    private static AudioInputStream stream;
    private static AudioFormat format;
    private static DataLine.Info info;
    private static Clip clip;
    private static Timer timer;
    private static TimerTask timerTask;

    public static void playSound(Sound sound) {
        try {
            bufferedIn = new BufferedInputStream(sound.getStream());    // Load into buffered stream for audiosystem to work (Needs functions only available when buffered)
            stream = AudioSystem.getAudioInputStream(bufferedIn);   // Load it
            format = stream.getFormat();
            info = new DataLine.Info(Clip.class, format);
            clip = (Clip) AudioSystem.getLine(info);
            clip.open(stream);
            //TODO: Fire Sound Events

            long frames = stream.getFrameLength();
            double durationInSeconds = (frames + 0.0) / format.getFrameRate();  // Calculate duration in seconds
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    SoundStopEvent soundStopEvent = new SoundStopEvent(sound);
                    EventManager.fireEvent(soundStopEvent);
                }
            };
            timer.schedule(timerTask, (int) (durationInSeconds * 1000));    // Fire event after the duration has passed

            clip.start();
            SoundStartEvent soundStartEvent = new SoundStartEvent(sound);
            EventManager.fireEvent(soundStartEvent);
            Logger.info("Playing sound " + sound.name());
        } catch (Exception e) {
            Logger.error("Error while playing sound: " + sound.name());
            e.printStackTrace();
        }
    }

    public static void playEffect(final Sound sound) {
        InputStream bufferedIn;
        AudioInputStream stream;
        AudioFormat format;
        DataLine.Info info;
        Clip clip;
        try {
            bufferedIn = new BufferedInputStream(sound.getStream());
            stream = AudioSystem.getAudioInputStream(bufferedIn);
            format = stream.getFormat();
            info = new DataLine.Info(Clip.class, format);
            clip = (Clip) AudioSystem.getLine(info);
            clip.open(stream);
            //TODO: Fire Sound Events

            long frames = stream.getFrameLength();
            double durationInSeconds = (frames + 0.0) / format.getFrameRate();
            Timer timer = new Timer();

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    SoundStopEvent soundStopEvent = new SoundStopEvent(sound);
                    EventManager.fireEvent(soundStopEvent);
                }
            }, ((int) (durationInSeconds * 1000) - 100));

            clip.start();
            SoundStartEvent soundStartEvent = new SoundStartEvent(sound);
            EventManager.fireEvent(soundStartEvent);
            Logger.info("Playing sound " + sound.name());
        } catch (Exception e) {
            Logger.error("Error while playing sound: " + sound.name());
            e.printStackTrace();
        }
    }

    //TODO: Emit SoundCancelEvent

    public static void stopSound() {
        if (clip != null) {
            timer.cancel();
            timer.purge();
            timerTask.cancel();
            clip.stop();
            Logger.info("Stopped sound");
        }
    }
}
