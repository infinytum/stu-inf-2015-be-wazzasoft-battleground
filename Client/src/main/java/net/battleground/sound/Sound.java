package net.battleground.sound;

import net.battleground.factory.AssetFactory;

import java.io.InputStream;

/**
 * Created by Michael on 17.03.2016.
 */
public enum Sound {
    BATTLE_INTRO {
        @Override
        public InputStream getStream() {
            return AssetFactory.loadSoundAsset("Battle_Intro.wav");
        }
    },
    BATTLE_MAIN {
        @Override
        public InputStream getStream() {
            return AssetFactory.loadSoundAsset("Battle_Main.wav");
        }
    },
    BUTTON_CLICK {
        @Override
        public InputStream getStream() {
            return AssetFactory.loadSoundAsset("Click.wav");
        }
    },
    HIT {
        @Override
        public InputStream getStream() {
            return AssetFactory.loadSoundAsset("Hit.wav");
        }
    };


    public abstract InputStream getStream();
}
