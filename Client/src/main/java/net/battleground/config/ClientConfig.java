package net.battleground.config;

import java.io.File;
import java.io.IOException;

/**
 * Created by bteusm on 31.03.2016.
 */
public class ClientConfig extends Configuration {


    public ClientConfig() {
        super("client.properties");

        File file = new File("client.properties");
        if (!file.exists()) {
            try {
                file.createNewFile();
                load();
                set("Server", "wfc.keecode.net");
                save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getServerHost() {
        return get("Server");
    }
}
