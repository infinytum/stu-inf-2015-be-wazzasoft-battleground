package net.battleground.client;

import io.netty.channel.Channel;
import net.battleground.ability.Ability;
import net.battleground.config.ClientConfig;
import net.battleground.entity.Player;
import net.battleground.network.Packet;
import net.battleground.network.PacketManager;
import net.battleground.packet.BattleAttackPacket;
import net.battleground.packet.LoginPacket;
import net.battleground.packet.LogoutPacket;
import net.battleground.packet.RegisterPacket;

/**
 * Created by mkteu on 25.03.2016.
 */
public class BattleClient {

    private static BattleClient client;
    private Channel serverChannel;
    private int clientID;
    private Player player;
    private ClientConfig config;

    private BattleClient() {
        config = new ClientConfig();
    }

    public static BattleClient getBattleClient() {
        if (BattleClient.client == null)
            BattleClient.client = new BattleClient();
        return client;
    }

    public static void login(String username, char[] password) {
        LoginPacket packet = new LoginPacket(username, new String(password));
        sendPacket(packet);
    }

    public static void register(String username, char[] password, char[] password_rep, String email) {
        RegisterPacket packet = new RegisterPacket(username, new String(password), new String(password_rep), email);
        sendPacket(packet);
    }

    public static void logout() {
        LogoutPacket packet = new LogoutPacket();
        sendPacket(packet);
        getBattleClient().serverChannel.disconnect();
    }

    public static void attack(Ability ability) {
        BattleAttackPacket packet = new BattleAttackPacket(ability);
        sendPacket(packet);
    }

    public static void sendPacket(Packet packet) {
        packet.setSender(getBattleClient().clientID);
        PacketManager.sendPacket(packet, getBattleClient().serverChannel);
    }

    public static Player getPlayer() {
        return getBattleClient().player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public void setServerChannel(Channel serverChannel) {
        this.serverChannel = serverChannel;
    }

    public ClientConfig getConfig() {
        return config;
    }
}
