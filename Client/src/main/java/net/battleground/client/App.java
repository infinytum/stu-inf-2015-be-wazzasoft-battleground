package net.battleground.client;

import net.battleground.event.EventManager;
import net.battleground.gui.screen.WelcomeScreen;
import net.battleground.listener.ServerListener;
import net.battleground.listener.SoundListener;
import net.battleground.network.PacketManager;
import net.battleground.network.ServerConnector;

/**
 * Created by bteusm on 11.03.2016.
 */
public class App {

    public static void main(String[] args) {

        ServerConnector sc = new ServerConnector();
        try {
            ServerListener serverListener = new ServerListener();
            EventManager.registerListener(serverListener);
            PacketManager.registerListener(serverListener);
            EventManager.registerListener(new SoundListener());
            BattleClient.getBattleClient();
            sc.start();

            WelcomeScreen screen = new WelcomeScreen();
            screen.setVisible(true);


            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    BattleClient.logout();
                }
            });
            sc.join();
        } catch (Exception ex) {
        }
    }


}
