package net.battleground.factory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Michael Teuscher
 * @version 1.0
 *          <p>
 *          The AssetFactory loads assets right from the JAR File
 */
public class AssetFactory {

    private static ClassLoader cl = AssetFactory.class.getClassLoader();
    private static String imageStorage = "assets/images/";
    private static String soundStorage = "assets/sound/";
    private static String langStorage = "assets/lang/";
    private static String asleacStorage = "assets/asleac/";
    private static String itemStorage = "assets/item/";

    // Different loaders for different image locations

    public static Image loadImageAsset(String assetName) {
        try {
            return ImageIO.read(loadAsset(imageStorage, assetName));
        } catch (IOException ex) {
            System.out.println("Error while loading asset \"" + assetName + "\"");
        }
        return null;
    }


    public static Image loadAsleacAsset(String assetName) {
        try {
            return ImageIO.read(loadAsset(asleacStorage, assetName));
        } catch (IOException ex) {
            System.out.println("Error while loading asset \"" + assetName + "\"");
        }
        return null;
    }


    public static Image loadItemAsset(String assetName) {
        try {
            return ImageIO.read(loadAsset(itemStorage, assetName));
        } catch (IOException ex) {
            System.out.println("Error while loading asset \"" + assetName + "\"");
        }
        return null;
    }

    public static InputStream loadSoundAsset(String assetName) {
        return loadAsset(soundStorage, assetName);
    }


    private static BufferedInputStream loadAsset(String assetLocation, String assetName) {
        InputStream is = cl.getResourceAsStream(assetLocation + assetName); // Getting a inputstream. Path is starting at root package

        if (is == null) {
            is = cl.getResourceAsStream(assetLocation + "NoTexture.png");   // If there is no such asset, give a no texture file
        }
        BufferedInputStream bis = new BufferedInputStream(is);
        return bis;
    }

}
