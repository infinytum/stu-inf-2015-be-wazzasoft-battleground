package net.battleground.network;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.battleground.client.BattleClient;
import net.battleground.event.EventManager;
import net.battleground.event.ServerConnectedEvent;

/**
 * Created by bteusm on 18.03.2016.
 */
public class BattleHandler extends SimpleChannelInboundHandler<Packet> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        BattleClient.getBattleClient().setServerChannel(ctx.channel());
        ServerConnectedEvent event = new ServerConnectedEvent();
        EventManager.fireEvent(event);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        PacketManager.handlePacket((Packet) msg);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet s) throws Exception {

    }
}
