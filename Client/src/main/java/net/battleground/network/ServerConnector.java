package net.battleground.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import net.battleground.client.BattleClient;

/**
 * Created by bteusm on 18.03.2016.
 */
public class ServerConnector extends Thread {

    private int port = 43110;
    private String hostname = "172.16.2.46"; // Change to WazzaSoft Global Server

    @Override
    public void run() {
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            final SslContext sslCtx = SslContext.newClientContext(InsecureTrustManagerFactory.INSTANCE);
            Bootstrap b = new Bootstrap(); // (1)
            b.group(workerGroup); // (2)
            b.channel(NioSocketChannel.class); // (3)
            b.option(ChannelOption.SO_KEEPALIVE, true); // (4)
            b.option(ChannelOption.SO_RCVBUF, 1024 * 1024 * 1024);
            b.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast("objectDecoder", new ObjectDecoder(1024 * 1024 * 1024, ClassResolvers.softCachingResolver(Packet.class.getClassLoader())));
                    ch.pipeline().addLast("objectEncoder", new ObjectEncoder());
                    ch.pipeline().addLast(new BattleHandler());
                }
            });

            // Start the client.
            ChannelFuture f = b.connect(BattleClient.getBattleClient().getConfig().getServerHost(), port).sync(); // (5)

            // Wait until the connection is closed.
            f.channel().closeFuture().sync();
        } catch (Exception ex) {

        } finally {
            workerGroup.shutdownGracefully();
        }
    }
}
